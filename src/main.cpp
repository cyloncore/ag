#include <iostream>

#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFileInfo>

#include "Error.h"
#include "Generator.h"
#include "Lexer.h"
#include "Parser.h"
#include "TypesManager.h"

int main(int _argc, char** _argv)
{
  QStringList include_dirs;
  int currentIndex = 1;
  while(currentIndex + 1 < _argc and QString(_argv[currentIndex]) == "-I")
  {
    include_dirs.append(_argv[currentIndex+1]);
    currentIndex += 2;
  }
  
  if(currentIndex + 3 > _argc)
  {
    qWarning() << "ag [-I <include_search_path>] qtcpp [input.rpd] [output]";
    return -1;
  }
  QString input_filename, output_filename;
  
  if(QString(_argv[currentIndex]) != "qtcpp")
  {
    std::cerr << "Unknown generator: '" << _argv[currentIndex] << "'\n";
  }
  input_filename  = _argv[currentIndex + 1];
  output_filename = _argv[currentIndex + 2];
  
  QFile file(input_filename);
  if(not file.open(QIODevice::ReadOnly))
  {
    std::cerr << "Failed to open '" << qPrintable(input_filename) << "'\n";
  }
  
  include_dirs.prepend(QFileInfo(input_filename).absoluteDir().absolutePath());
  
  Lexer lexer(&file);
  TypesManager typesManager(nullptr);
  Parser parser(&lexer, &typesManager, input_filename, include_dirs);
  auto[ definition, imported_headers] = parser.parse();
  
  if(not definition.empty())
  {
    Generator gen;
    gen.generate(&typesManager, definition, output_filename, imported_headers);
    return 0;
  } else {
    QList<Error> errors = parser.errors();
    std::cout << "Compilation failed, with " << errors.size() << " error(s): " << std::endl;
    foreach(const Error& error, errors)
    {
      std::cout << qPrintable(error.filename) << ":" << error.line << ":" << error.column << ": error:" << qPrintable(error.message) << std::endl;
    }
    return -1;
  }
  
  return -1;
}
