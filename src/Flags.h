#include "Type.h"

class ClassDefinition;

class Flags : public Type
{
public:
  Flags(const QString& _flags_name, const Enum* _enum, const ClassDefinition* _parent);
  ~Flags();
  const ClassDefinition* parent() const;
  const Enum* enumType() const;
private:
  struct Private;
  Private* const d;
};

