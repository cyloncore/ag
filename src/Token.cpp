/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Token.h"

#include <QDebug>

const char* Token::typeToString(Token::Type _type)
{
  switch(_type)
  {
    case UNFINISHED_STRING:
      return "unfinished string";
    case UNFINISHED_COMMENT:
      return "unfinished comment";
    case END_OF_FILE:
      return "end of file";
    case UNKNOWN:
      return "unknown";
    case SEMI:
      return ";";
    case STARTBRACE:
      return "{";
    case ENDBRACE:
      return "}";
    case STARTBRACKET:
      return "(";
    case ENDBRACKET:
      return ")";
    case COMMA:
      return ",";
    case DOT:
      return ".";
    case ARROW:
      return "->";
    case COLONCOLON:
      return "::";
    case EQUAL:
      return "=";
    case IDENTIFIER:
      return "identifier";
    case STRING:
      return "string";
    case CLASS:
      return "class";
    case NAMESPACE:
      return "namespace";
    case LIST:
      return "list";
    case OF:
      return "of";
    case PRIVATE:
      return "private";
    case PROTECTED:
      return "protected";
    case PUBLIC:
      return "public";
    case INVOKABLE:
      return "invokable";
    case ENUM:
      return "enum";
    case IMPLEMENTS:
      return "implements";
    case INHERITS:
      return "inherits";
    case CONST:
      return "const";
    case READONLY:
      return "readonly";
    case UNSERIALISABLE:
      return "unserialisable";
    case TYPEDEF:
      return "typedef";
    case FROM:
      return "from";
    case KEY:
      return "key";
    case REFERENCE:
      return "reference";
    case WITH:
      return "with";
    case SET:
      return "set";
    case UNSERIALISE:
      return "unserialise";
    case FLAGS:
      return "flags";
    case OPTIONAL:
      return "optional";
    case IMPORT:
      return "import";
    case EXTERNAL:
      return "external";
    case VECTOR:
      return "vector";
    case VIRTUAL:
      return "virtual";
    case PUREVIRTUAL:
      return "pure_virtual";
    case OVERRIDE:
      return "override";
  }
  qFatal("add the token");
}

Token::Token() : type(UNKNOWN)
{

}

Token::Token(Type _type, int _line, int _column) : type(_type), line(_line), column(_column)
{
}

Token::Token(Type _type, const QString& _string, int _line, int _column) : type(_type), line(_line), column(_column), string(_string)
{
  Q_ASSERT( _type == IDENTIFIER or _type == STRING or _type == UNKNOWN );
}
