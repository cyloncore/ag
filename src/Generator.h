
#include <QList>

class QString;
class ClassDefinition;
class TypesManager;

class Generator
{
public:
  Generator();
  ~Generator();
  void generate(const TypesManager* _typesManager, const QList<ClassDefinition*> _definitions, const QString& _output, const QStringList& _importedHeaders);
};
