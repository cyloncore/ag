#include <QObject>

namespace test
{
  class Test1;
  class Test2;
}

class TestAg : public QObject
{
  Q_OBJECT
public:
  explicit TestAg(QObject* parent = 0);
private:
  test::Test1* createTest1(int _offset);
  void fillTest1(test::Test1* _test1, int _offset);
  test::Test2* createTest2(int _offset);
  void fillTest2(test::Test2* _test2, int _offset);
private slots:
  void testTest1();
  void testTestBuilder1();
  void testTest2();
  void testTest3();
  void testTestBuilder3();
  void testTest4();
  void testSerialisation();
  void testLoadable();
  void testSerialisationReference567();
  void testFlags();
  void testExternal();
  void testInvokable();
  void testIniitilalise();
  void testVirtual();
  void testClone();
  void testNullReference();
};

