#include "/%= _output %/.h"

#include <QJsonObject>
#include <QVariant>

/%

for(const ClassDefinition* cd : _definitions)
{
  if(not cd->namespaces().isEmpty())
  {
%/
namespace /%= cd->namespaces().join("::") %/
{/%
  }
%/
  struct /%= cd->name() %/::__ag_Data__
  {
  /%
    if(implementsLoadable(cd))
    {%/
    QString filename, errorMessage;/%
    }
    for(const ClassDefinition::Field& field : cd->fields())
    {
      %/
    /%= typenameMember(field.type, cd) %/ /%= field.name %/ = ag::initValue</%= trueOrFalse(isReferenceOrExternal(field)) %/, /%= typenameMember(field.type, cd) %/>();/%
    }
    %/
    static QHash<QString, __ag_Factory__*> __ag_factories__;
  };

  QHash<QString, /%= cd->name() %/::__ag_Factory__*> /%= cd->name() %/::__ag_Data__::__ag_factories__;
  
  /%= cd->name() %/::/%= cd->name() %/(QObject* _parent) : /%= parentClass(cd) %/(/%
    if(cd->parent())
    {
      for(const ClassDefinition::Field& field : protectedInitialiserFields(cd->parent()))
      {
        QString initialiser = cd->initialiser(field.name);
        if(initialiser.isEmpty()) { initialiser = field.initialiser; }
        if(initialiser.isEmpty())
        {
          %/ag::initValue</%= trueOrFalse(isReferenceOrExternal(field)) %/, /%= typenameMember(field.type, cd) %/>(),/%
        } else {
        %//%= initialiser %/, /%
        }
      }
    }
  %/_parent), _data_(new __ag_Data__)
  {/%

  for(const ClassDefinition::Field& field : cd->fields())
  {
    if(not field.initialiser.isEmpty())
    {%/
    _data_->/%= field.name %/ = /%= field.initialiser %/;/%
    }
    if(field.type->type() == Type::TType::Class and not isReferenceOrExternal(field))
    {
    %/
    _data_->/%= field.name %/->setParent(this);/%
    }
    if(cd->hasFunction(QString("on%1Changed").arg(upcase(field.name))))
    {
      %/
    on/%= upcase(field.name) %/Changed();/%
    }
  }
  QHash<QString, QString> initialisers = cd->initialisers();
  for(auto it = initialisers.begin(); it != initialisers.end(); ++it)
  {
    if(not cd->field(it.key()).tags.testFlag(ClassDefinition::Field::Tag::Const))
    {
      %/
    set/%= upcase(it.key())%/(/%= it.value() %/);/%
    }
  }
  if(cd->hasFunction("initialise"))
  {%/
  initialise();/%
  }
%/
  }/%
  if(hasProtectedInitialiser(cd))
  {%/
  /%= cd->name() %/::/%= cd->name() %/(/%= protectedInitialiserArguments(cd) %/QObject* _parent) : /%= parentClass(cd) %/(/%
    if(cd->parent())
    {
      for(const ClassDefinition::Field& field : protectedInitialiserFields(cd->parent()))
      {
        %/_/%= field.name%/, /%
      }
    }
  %/_parent), _data_(new __ag_Data__)
  {/%

  for(const ClassDefinition::Field& field : cd->fields())
  {
    if(field.tags.testFlag(ClassDefinition::Field::Tag::Const))
    {%/
    _data_->/%= field.name %/ = _/%= field.name %/;/%
    } else if(not field.initialiser.isEmpty())
    {%/
    _data_->/%= field.name %/ = /%= field.initialiser %/;/%
    }
    if(field.type->type() == Type::TType::Class and not isReferenceOrExternal(field))
    {
    %/
    _data_->/%= field.name %/->setParent(this);/%
    }
    if(cd->hasFunction(QString("on%1Changed").arg(upcase(field.name))))
    {
      %/
    on/%= upcase(field.name) %/Changed();/%
    }
  }
  QHash<QString, QString> initialisers = cd->initialisers();
  for(auto it = initialisers.begin(); it != initialisers.end(); ++it)
  {
    if(not cd->field(it.key()).tags.testFlag(ClassDefinition::Field::Tag::Const))
    {
      %/
    set/%= upcase(it.key())%/(/%= it.value() %/);/%
    }
  }
  if(cd->hasFunction("initialise"))
  {%/
  initialise();/%
  }
%/
  }/%
  }%/
  /%= cd->name() %/::~/%= cd->name() %/()
  {
  /%
  for(const ClassDefinition::Field& field : cd->fields())
  {%/
    ag::clean</%= trueOrFalse(isReferenceOrExternal(field))%/>(&_data_->/%= field.name %/);/%
  }%/
    delete _data_;
  }
  /%
  if(cd->pureVirtualFunctions().isEmpty())
  {
  %/  
  /%= typenameResolved(cd->cloneType(), cd) %/ * /%= cd->name() %/::clone() const
  {
    /%= cd->name() %/ * o = new /%= cd->name() %/;
    fillClone(o);
    return o;
  }
  /%
  }
  // end of pureVirtualFunctions check
  %/
  void /%= cd->name() %/::fillClone(/%= cd->name() %/ * _destination) const
  {
/%
  if(cd->fields().isEmpty() and not cd->parent())
  {
    %/
  Q_UNUSED(_destination);/%
  }
  for(const ClassDefinition::Field& field : cd->fields())
  {%/
    _destination->_data_->/%= field.name %/ = ag::cloneValue</%= trueOrFalse(isReferenceOrExternal(field)) %/>(_data_->/%= field.name %/);/%
  }
  if(cd->parent())
  {
    %/
    /%= typenameResolved(cd->parent(), cd) %/::fillClone(_destination);/%
  }
  %/
  }

  /%
    if(implementsLoadable(cd))
    {%/
  // Loadable interface
  bool /%= cd->name() %/::load(/%= argumentList(cd, cd->unserialisationArguments(), false, true, true) %/QString* _errorMessage)
  {
    QFile file(_data_->filename);
    if(not file.open(QIODevice::ReadOnly))
    {
      _data_->errorMessage = QString("Failed to open file '%1' for reading.").arg(_data_->filename);
      emit(errorMessageChanged());
      if(_errorMessage)
      {
        *_errorMessage = _data_->errorMessage;
      }
      return false;
    }
    QJsonParseError pe;
    QJsonDocument document = QJsonDocument::fromJson(file.readAll(), &pe);
    if(document.isNull())
    {
      _data_->errorMessage = QString("Failed to parse file '%1':").arg(_data_->filename).arg(pe.errorString());
      emit(errorMessageChanged());
      if(_errorMessage)
      {
        *_errorMessage = _data_->errorMessage;
      }
      return false;
    }
    QJsonObject object = document.object();
    QString eMsg;
    bool ret = loadFromJson(object/%= argumentNameList(cd->unserialisationArguments(), true, true) %/, &eMsg);
    if(eMsg != _data_->errorMessage)
    {
      _data_->errorMessage = eMsg;
      emit(errorMessageChanged());
    }
    if(_errorMessage)
    {
      *_errorMessage = _data_->errorMessage;
    }
    return ret;
  }
  bool /%= cd->name() %/::save()
  {
    QFile file(_data_->filename);
    if(not file.open(QIODevice::WriteOnly))
    {
      return false;
    }
    QJsonValue val = toJson();
    QJsonDocument document = val.isArray() ? QJsonDocument(val.toArray()) : QJsonDocument(val.toObject());
    file.write(document.toJson());
    return true;
  }
  QString /%= cd->name() %/::filename() const
  {
    return _data_->filename;
  }
  QString /%= cd->name() %/::errorMessage() const
  {
    return _data_->errorMessage;
  }
  void /%= cd->name() %/::setFilename(const QString& _filename)
  {
    _data_->filename = _filename;
    emit(filenameChanged());
    /%
    if(implementsAutoload(cd))
    {%/
    QString errMsg;
    if(load(&errMsg))
    {
      emit(autoloadSucceeded());
    } else {
      emit(autoloadFailed(errMsg));
    }/%
    }
    %/
  }
/%
    }
  %/
  
  /%
  for(const ClassDefinition::Field& field : cd->fields())
  {
    %/
  /%= typenameClassGetterReturn(field.type, nullptr) %/ /%= cd->name() %/::/%= field.name %/() const
  {
    return _data_->/%= field.name %/;
  }/%
    if(hasSetter(field))
    {
    %/  
  void /%= cd->name() %/::set/%= upcase(field.name) %/(/%= typenameArgument(field.type, cd) %/ _value)
  {
    ag::clean</%= trueOrFalse(isReferenceOrExternal(field))%/>(&_data_->/%= field.name %/);
    _data_->/%= field.name %/ = _value;
    /%
      if(cd->hasFunction(QString("on%1Changed").arg(upcase(field.name))))
      {
        %/
    on/%= upcase(field.name) %/Changed();/%
      }
    %/
    emit(changed());
    emit(/%= field.name %/Changed());
  }/%
      if(hasAppend(field))
      {%/
  void /%= cd->name() %/::appendTo/%= upcase(field.name) %/(/%= typenameArgument(field.type->nested(), cd) %/ _value)
  {
    _data_->/%= field.name %/.append(_value);
    /%
      if(cd->hasFunction(QString("on%1Changed").arg(upcase(field.name))))
      {
        %/
    on/%= upcase(field.name) %/Changed();/%
      }
    %/
    emit(changed());
    emit(/%= field.name %/Changed());
  }/%
      }
  
  
    } else if(hasBuilder(field))
      {
        %/
void /%= cd->name() %/::set/%= upcase(field.name) %/Builder(/%= typenameResolved(field.type, cd) %/::Builder&& _value)
{
  delete _data_->/%= field.name %/;
  _data_->/%= field.name %/ = _value;
}
        /%
      }
  }
  
  for(const ClassDefinition::Field& field : cd->fields())
  {
    if(shouldHaveQListQObject(field))
    {%/
  QList<QObject*> /%= cd->name() %/::/%= field.name %/AsQListQObject() const
  {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-aliasing"
    return *reinterpret_cast<const QList<QObject*>*>(&_data_->/%= field.name %/);
#pragma GCC diagnostic pop
  }
  void /%= cd->name() %/::set/%= upcase(field.name) %/(const QList<QObject*>& _value)
  {
    ag::clean</%= trueOrFalse(isReferenceOrExternal(field))%/>(&_data_->/%= field.name %/);
    for(QObject* o : _value)
    {
      /%= typenameMember(field.type->nested(), cd) %/ a = qobject_cast</%= typenameMember(field.type->nested(), cd) %/>(o);
      if(a)
      {
        _data_->/%= field.name %/.append(a);
      } else {
        o->deleteLater();
      }
    }
    emit(/%= field.name %/Changed());
  }/%
    }
    if(shouldHaveQVectorQObject(field))
    {%/
  QVector<QObject*> /%= cd->name() %/::/%= field.name %/AsQVectorQObject() const
  {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-aliasing"
    return *reinterpret_cast<const QVector<QObject*>*>(&_data_->/%= field.name %/);
#pragma GCC diagnostic pop
  }
  void /%= cd->name() %/::set/%= upcase(field.name) %/(const QVector<QObject*>& _value)
  {
    ag::clean</%= trueOrFalse(isReferenceOrExternal(field))%/>(&_data_->/%= field.name %/);
    for(QObject* o : _value)
    {
      /%= typenameMember(field.type->nested(), cd) %/ a = qobject_cast</%= typenameMember(field.type->nested(), cd) %/>(o);
      if(a)
      {
        _data_->/%= field.name %/.append(a);
      } else {
        o->deleteLater();
      }
    }
    emit(/%= field.name %/Changed());
  }/%
    }    if(shouldHaveQVariantHash(field))
    {
    %/
    QVariant /%= cd->name() %/::/%= field.name %/AsQVariant() const
    {
      return _data_->/%= field.name %/.toVariantMap();
    }
    void /%= cd->name() %/::set/%= upcase(field.name) %/(const QVariant& _value)
    {
      ag::clean</%= trueOrFalse(isReferenceOrExternal(field))%/>(&_data_->/%= field.name %/);
      _data_->/%= field.name %/ = /%= typenameMember(field.type, cd) %/::fromVariant(_value);
    }/%
    }
  }
  
  %/
  
  // MD5
  QByteArray /%= cd->name() %/::hash(QCryptographicHash::Algorithm _alg) const
  {
    QCryptographicHash h(_alg);
    hash(&h);
    return h.result();
  }
  void /%= cd->name() %/::hash(QCryptographicHash* _hash) const
  {
    /%
  if(cd->fields().isEmpty())
  {
    %/
    Q_UNUSED(_hash);/%
  }
  if(cd->parent())
  {
    %//%= parentClass(cd) %/::hash(_hash);/%
  }
  for(const ClassDefinition::Field& field : cd->fields())
  {
    %/
    ag::hash</%= trueOrFalse(isReferenceOrExternal(field)) %/>(_hash, _data_->/%= field.name %/);/%
  }  
  %/
  }

  // Json Serialisation

  QJsonObject /%= cd->name() %/::toJson() const
  {
    QJsonObject obj/%
  if(cd->parent())
  {
    %/ = /%= parentClass(cd) %/::toJson();/%
  } else {
    %/;/%
  }%/
  obj["__klass__"] = "/%= typenameResolved(cd, nullptr) %/";/%
  for(const ClassDefinition::Field& field : cd->fields())
  {
    if(not field.tags.testFlag(ClassDefinition::Field::Tag::Unserialisable) and not field.tags.testFlag(ClassDefinition::Field::Tag::External))
    {
    %/
    obj["/%= field.name %/"] = ag::toJson</%= trueOrFalse(field.tags.testFlag(ClassDefinition::Field::Tag::Reference)) %/>(_data_->/%= field.name %/);/%
    }
  }
  %/
    return obj;
  }
  bool /%= cd->name() %/::loadFromJson(const QJsonObject& _object/%= argumentList(cd, cd->unserialisationArguments(), true, false, true) %/, QString* _errorMessage)
  {/%
  bool allFieldsAreExternal = true;
  for(const ClassDefinition::Field& field : cd->fields())
  {
    if(not field.tags.testFlag(ClassDefinition::Field::Tag::External))
    {
      allFieldsAreExternal = false;
      break;
    }
  }
  if(cd->fields().isEmpty() or allFieldsAreExternal)
  {
    %/
    Q_UNUSED(_object);
    Q_UNUSED(_errorMessage);/%
  }
  if(cd->parent())
  {
    %/
    if(not /%= parentClass(cd) %/::loadFromJson(_object/%= argumentNameList(cd->parent()->unserialisationArguments(), true, true) %/, _errorMessage))
    {
      return false;
    }/%
  }
  for(const ClassDefinition::Field& field : cd->fields())
  {
    if(field.tags.testFlag(ClassDefinition::Field::Tag::Unserialisable))
    { // do nothing
    } else if(field.tags.testFlag(ClassDefinition::Field::Tag::External))
    {
      %/
    _data_->/%= field.name %/ = /%= field.unserialiseWith.first() %/;/%
    } else
    {
      if(not field.initialiser.isEmpty())
      {
      %/
    if(not _object.contains("/%= field.name %/"))
    {
      _data_->/%= field.name %/ = /%= field.initialiser %/;
    } else
        /%
      }
      %/
    if(not ag::fromJson</%= trueOrFalse(isReferenceOrExternal(field)) %/>(&_data_->/%= field.name %/, _object["/%= field.name %/"], _errorMessage/%= argumentNameList(field.unserialiseWith, cd, true) %/))
    {
      if(_errorMessage)
      {
        *_errorMessage = QString("Cannot read value for field '/%= field.name %/' with error '%2'").arg(*_errorMessage);
      }
      return false;
    }/%
      if(cd->hasFunction(QString("on%1Changed").arg(upcase(field.name))))
      {
      %/
    on/%= upcase(field.name) %/Changed();/%
      }%/
    emit(/%= field.name %/Changed());/%
    }
  }
  %/
    return true;
  }
  /%= cd->name() %/ * /%= cd->name() %/::fromJson(const QJsonObject& _object/%= argumentList(cd, cd->unserialisationArguments(), true, false, false) %/, QString* _errorMessage)
  {
    QString __klass_name__ = _object.value("__klass__").toString();
    if(__klass_name__.isEmpty() or __klass_name__ == "/%= typenameResolved(cd, nullptr) %/")
    {
    /%
    if(cd->pureVirtualFunctions().isEmpty())
    {
    %/
      /%= cd->name() %/ * v = new /%= cd->name() %/();
      if(v->loadFromJson(_object/%= argumentNameList(cd->unserialisationArguments(), true, true) %/, _errorMessage))
      {
        return v;
      } else {
        delete v;
        if(_errorMessage)
        {
          *_errorMessage = QString("Failed to load '/%= typenameResolved(cd, nullptr) %/': %1").arg(*_errorMessage);
        }
        return nullptr;
      }
      /%
    } else {%/
      if(_errorMessage)
      {
        *_errorMessage =QString("/%= cd->name() %/ is a virtual class and cannot be loaded");
      }
      return nullptr;
    /%
    }
    %/
    }
    __ag_Factory__* f = __ag_Data__::__ag_factories__.value(__klass_name__);
    if(f)
    {
      return f->fromJson(_object/%= argumentNameList(cd->unserialisationArguments(), true, false) %/, _errorMessage);
    }
    if(_errorMessage)
    {
      *_errorMessage = QString("No class name '%1' inherits from '/%= typenameResolved(cd, nullptr) %/'").arg(__klass_name__);
    }
    return nullptr;
  }

  // Cbor Serialisation

  QCborMap /%= cd->name() %/::toCbor() const
  {
    QCborMap obj/%
  if(cd->parent())
  {
    %/ = /%= parentClass(cd) %/::toCbor();/%
  } else {
    %/;/%
  }%/
  obj[QStringLiteral("__klass__")] = "/%= typenameResolved(cd, nullptr) %/";/%
  for(const ClassDefinition::Field& field : cd->fields())
  {
    if(not field.tags.testFlag(ClassDefinition::Field::Tag::Unserialisable) and not field.tags.testFlag(ClassDefinition::Field::Tag::External))
    {
    %/
    obj[QStringLiteral("/%= field.name %/")] = ag::toCbor</%= trueOrFalse(field.tags.testFlag(ClassDefinition::Field::Tag::Reference)) %/>(_data_->/%= field.name %/);/%
    }
  }
  %/
    return obj;
  }
  bool /%= cd->name() %/::loadFromCbor(const QCborMap& _object/%= argumentList(cd, cd->unserialisationArguments(), true, false, true) %/, QString* _errorMessage)
  {/%
  if(cd->fields().isEmpty() or allFieldsAreExternal)
  {
    %/
    Q_UNUSED(_object);
    Q_UNUSED(_errorMessage);/%
  }
  if(cd->parent())
  {
    %/
    if(not /%= parentClass(cd) %/::loadFromCbor(_object/%= argumentNameList(cd->parent()->unserialisationArguments(), true, true) %/, _errorMessage))
    {
      return false;
    }/%
  }
  for(const ClassDefinition::Field& field : cd->fields())
  {
    if(field.tags.testFlag(ClassDefinition::Field::Tag::Unserialisable))
    { // do nothing
    } else if(field.tags.testFlag(ClassDefinition::Field::Tag::External))
    {
      %/
    _data_->/%= field.name %/ = /%= field.unserialiseWith.first() %/;/%
    } else
    {
      if(not field.initialiser.isEmpty())
      {
      %/
    if(not _object.contains(QStringLiteral("/%= field.name %/")))
    {
      _data_->/%= field.name %/ = /%= field.initialiser %/;
    } else
        /%
      }
      %/
    if(not ag::fromCbor</%= trueOrFalse(isReferenceOrExternal(field)) %/>(&_data_->/%= field.name %/, _object[QStringLiteral("/%= field.name %/")], _errorMessage/%= argumentNameList(field.unserialiseWith, cd, true) %/))
    {
      if(_errorMessage)
      {
        *_errorMessage = QString("Cannot read value for field '/%= field.name %/' with error '%2'").arg(*_errorMessage);
      }
      return false;
    }/%
      if(cd->hasFunction(QString("on%1Changed").arg(upcase(field.name))))
      {
      %/
    on/%= upcase(field.name) %/Changed();/%
      }
      %/
    emit(/%= field.name %/Changed());/%
    }
  }
  %/
    return true;
  }
  /%= cd->name() %/ * /%= cd->name() %/::fromCbor(const QCborMap& _object/%= argumentList(cd, cd->unserialisationArguments(), true, false, false) %/, QString* _errorMessage)
  {
    QString __klass_name__ = _object.value("__klass__").toString();
    if(__klass_name__.isEmpty() or __klass_name__ == "/%= typenameResolved(cd, nullptr) %/")
    {
    /%
    if(cd->pureVirtualFunctions().isEmpty())
    {
    %/
      /%= cd->name() %/ * v = new /%= cd->name() %/();
      if(v->loadFromCbor(_object/%= argumentNameList(cd->unserialisationArguments(), true, true) %/, _errorMessage))
      {
        return v;
      } else {
        delete v;
        if(_errorMessage)
        {
          *_errorMessage = QString("Failed to load '/%= typenameResolved(cd, nullptr) %/': %1").arg(*_errorMessage);
        }
        return nullptr;
      }
      /%
    } else {%/
      if(_errorMessage)
      {
        *_errorMessage =QString("/%= cd->name() %/ is a virtual class and cannot be loaded");
      }
      return nullptr;
    /%
    }
    %/
    }
    __ag_Factory__* f = __ag_Data__::__ag_factories__.value(__klass_name__);
    if(f)
    {
      return f->fromCbor(_object/%= argumentNameList(cd->unserialisationArguments(), true, false) %/, _errorMessage);
    }
    if(_errorMessage)
    {
      *_errorMessage = QString("No class name '%1' inherits from '/%= typenameResolved(cd, nullptr) %/'").arg(__klass_name__);
    }
    return nullptr;
  }
/%
  if(cd->parent())
  {
    %/
  class /%= cd->name() %/::__ag_FactoryImplementation__ : public /%= typenameResolved(cd->parent(), nullptr) %/::__ag_Factory__
  {
  public:
    /%= cd->name() %/ * fromJson(const QJsonObject&_object/%= argumentList(cd, cd->parent()->unserialisationArguments(), true, false, false) %/, QString* _errorMessage) override
    {
      return /%= cd->name() %/::fromJson(_object/%= argumentNameList(cd->unserialisationArguments(), true, false) %/, _errorMessage);
    }
    /%= cd->name() %/ * fromCbor(const QCborMap&_object/%= argumentList(cd, cd->parent()->unserialisationArguments(), true, false, false) %/, QString* _errorMessage) override
    {
      return /%= cd->name() %/::fromCbor(_object/%= argumentNameList(cd->unserialisationArguments(), true, false) %/, _errorMessage);
    }
  };
    /%
  }
  %/
  /%= cd->name() %/::__ag_Factory__::~__ag_Factory__()
  {
  }
  void /%= cd->name() %/::registerFactory(const QString& _name, __ag_Factory__* _factory)
  {
    __ag_Data__::__ag_factories__[_name] = _factory;
    /%
    if(cd->parent())
    {%/
    /%= typenameResolved(cd->parent(), nullptr) %/::registerFactory(_name, _factory);/%
    }
    %/
  }
  void /%= cd->name() %/::unregisterFactory(const QString& _name)
  {
    __ag_Data__::__ag_factories__.remove(_name);
    /%
    if(cd->parent())
    {%/
    /%= typenameResolved(cd->parent(), nullptr) %/::unregisterFactory(_name);/%
    }
    %/
  }
  /%
  if(cd->parent())
  {%/
  class /%= cd->name() %/::__ag_Registration__
  {
  public:
    __ag_Registration__() : m_factory(new __ag_FactoryImplementation__)
    {
      /%= typenameResolved(cd->parent(), nullptr) %/::registerFactory("/%= typenameResolved(cd, nullptr) %/", m_factory);
    }
    ~__ag_Registration__()
    {
      /%= typenameResolved(cd->parent(), nullptr) %/::unregisterFactory("/%= typenameResolved(cd, nullptr) %/");
      delete m_factory;
    }
  private:
    __ag_FactoryImplementation__* m_factory;
  };
  /%= cd->name() %/::__ag_Registration__ /%= cd->name() %/::__ag_staticRegistration__;
  /%
  }

  if(not cd->namespaces().isEmpty())
  {
%/
}
/%
  }
%/
namespace ag
{
/%
  for(const Enum* e : cd->enums())
  {
    QString e_typename = cd->namespaces().join("::") + "::" + cd->name() + "::" + e->name();
    %/
  template<>
  QString serialise<QString, /%= e_typename %/>(/%= e_typename %/ _value)
  {
    switch(_value)
    {/%
      for(const QString& val : allEnumValues(e))
      {%/
      case /%= e_typename %/::/%= val %/: return QStringLiteral("/%= val %/");
        /%
      }
      %/
    }
    return QString();
  }
  template<>
  bool unserialise</%= e_typename %/>(const QString& _string, /%= e_typename %/ * _value)
  {
    /%
      for(const QString& val : allEnumValues(e))
      {%/
    if(_string == "/%= val %/")
    {
      *_value = /%= e_typename %/::/%= val %/;
      return true;
    }/%
      }
      %/
    return false;
  }/%
  }
  for(const Flags* f : cd->flags())
  {
    QString f_typename = cd->namespaces().join("::") + "::" + cd->name() + "::" + f->name();
    QString e_typename = cd->namespaces().join("::") + "::" + cd->name() + "::" + f->name();
    %/
  template<>
  QStringList serialise<QStringList, /%= f_typename %/>(/%= f_typename %/ _value)
  {
    QStringList array;
    if(int(_value) == int(/%= typenameMember(f->enumType(), nullptr) %/::All))
    {
      array.append("All");
      return array;
    } else if(int(_value) == int(/%= typenameMember(f->enumType(), nullptr) %/::None))
    {
      return array;
    }
    /%
    for(const QString& val : f->enumType()->values())
    {%/
      if(_value.testFlag(/%= typenameMember(f->enumType(), nullptr) %/::/%= val %/))
      {
        array.append("/%= val %/");
      }/%
    }%/
    return array;
  }
  template<>
  bool unserialise</%= f_typename %/>(const QStringList& _string, /%= f_typename %/ * _value)
  {
    *_value = /%= typenameMember(f->enumType(), nullptr) %/::None;
    for(const QString& val_s : _string)
    {
    /%
      for(const QString& val : allEnumValues(f->enumType()))
      {%/if(val_s == "/%= val %/")
    {
      _value->setFlag(/%= typenameMember(f->enumType(), nullptr) %/::/%= val %/, true);
    } else /%
      }
      %/{
        return false;
      }
    }
    return true;
  }/%
  }%/

}  
/%
}
%/

#include "moc_/%= QFileInfo(_output).baseName() %/.cpp"
