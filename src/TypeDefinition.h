#include "Type.h"

class TypeDefinition : public Type
{
  friend class Parser;
public:
  struct UnserialisationArgument
  {
    QString name;
    const Type* type;
  };

  TypeDefinition(const QString& _name, const QString& _cppName, const QString& _cppArgumentName, const QString& _header);
  ~TypeDefinition();

  QList<UnserialisationArgument> unserialisationArguments() const;

private:
  struct AddResult {
    bool success;
    QString errorMessage;
  };
  inline static AddResult tdSuccess();
  inline static AddResult tdFailure(const QString& _error);
  AddResult addUnserialiseArguments(const QString& _name, const Type* _type);
private:
  struct Private;
  Private* const d;
};
