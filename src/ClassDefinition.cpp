/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "ClassDefinition.h"

#include <QHash>

#include "Enum.h"
#include "Flags.h"
#include "TypeDefinition.h"
#include "TypesManager.h"

struct ClassDefinition::Private
{
  bool isForward = false;
  QStringList nameSpace, interfaces;
  QString name;
  TypesManager* typesManager = nullptr;
  QList<Field> fields;
  QList<Function> calls;
  QList<const Enum*> enums;
  QList<const Flags*> flags;
  const ClassDefinition* parent = nullptr;
  QHash<QString, QString> initilisers;
  Field keyField;
  QList<UnserialisationArgument> unserialisationArguments;
  const Enum* addEnum(const QString& _name, ClassDefinition* _self, const QStringList& _values, bool _is_flag);
};

ClassDefinition::ClassDefinition(const QString& _name, const QStringList& _namespace, const TypesManager* _namespace_manager) : Type(_name, _namespace.join("::"), TType::Class), d(new Private)
{
  d->name       = _name;
  d->nameSpace  = _namespace;
  d->isForward  = true;
  d->typesManager = new TypesManager(_namespace_manager);
}

ClassDefinition::ClassDefinition(const QString& _name, const QStringList& _namespace, const QStringList& _interfaces, const ClassDefinition* _parent, const TypesManager* _namespace_manager) : Type(_name, _namespace.join("::"), TType::Class), d(new Private)
{
  d->name       = _name;
  d->nameSpace  = _namespace;
  d->interfaces = _interfaces;
  d->parent     = _parent;
  d->typesManager = new TypesManager(_namespace_manager);
  if(d->parent)
  {
    d->unserialisationArguments = d->parent->unserialisationArguments();
  }
}

ClassDefinition::~ClassDefinition()
{
  delete d->typesManager;
  delete d;
}

bool ClassDefinition::isForwardDefinition() const
{
  return d->isForward;
}

bool ClassDefinition::swap(ClassDefinition* _cd)
{
  if(isForwardDefinition())
  {
    std::swap(d, _cd->d);
    return true;
  } else {
    return false;
  }
}

const TypesManager* ClassDefinition::typesManager() const
{
  return d->typesManager;
}

const ClassDefinition* ClassDefinition::parent() const
{
  return d->parent;
}

const ClassDefinition* ClassDefinition::cloneType() const
{
  return d->parent ? d->parent->cloneType() : this;
}

QStringList ClassDefinition::namespaces() const
{
  return d->nameSpace;
}

QStringList ClassDefinition::interfaces() const
{
  return d->interfaces;
}

QString ClassDefinition::name() const
{
  return d->name;
}

QList<const Enum*> ClassDefinition::enums() const
{
  return d->enums;
}

QList<const Flags *> ClassDefinition::flags() const
{
  return d->flags;
}

QList<ClassDefinition::Field> ClassDefinition::fields() const
{
  return d->fields;
}

bool ClassDefinition::hasKey() const
{
  return not d->keyField.name.isEmpty();
}

ClassDefinition::Field ClassDefinition::keyField() const
{
  return d->keyField;
}

QList<ClassDefinition::Function> ClassDefinition::functions() const
{
  return d->calls;
}

bool ClassDefinition::hasFunction(const QString& _name) const
{
  for(const Function& f : d->calls)
  {
    if(f.name == _name)
    {
      return true;
    }
  }
  return false;
}

QString ClassDefinition::initialiser(const QString& _initialiser) const
{
  if(d->initilisers.contains(_initialiser)) { return d->initilisers.value(_initialiser); }
  else if(d->parent) { return d->parent->initialiser(_initialiser); }
  else { return QString(); }
}

QHash<QString, QString> ClassDefinition::initialisers() const
{
  return d->initilisers;
}

ClassDefinition::Field ClassDefinition::field(const QString& _name) const
{
  for(const Field& f : d->fields)
  {
    if(f.name == _name) return f;
  }
  if(d->parent)
  {
    return d->parent->field(_name);
  }
  return {};
}

bool ClassDefinition::hasField(const QString& _name) const
{
  for(const Field& f : d->fields)
  {
    if(f.name == _name) return true;
  }
  if(d->parent)
  {
    return d->parent->hasField(_name);
  }
  return false;
}

ClassDefinition::AddResult ClassDefinition::cdSuccess()
{
  return {true, QString()};
}

ClassDefinition::AddResult ClassDefinition::cdFailure(const QString& _error)
{
  return {false, _error};
}

ClassDefinition::AddResult ClassDefinition::addInitialiser(const QString& _name, const QString& _initialiser)
{
  d->initilisers[_name] = _initialiser;
  return cdSuccess();
}

#define RETURN_FAILURE(_EXPR_) { AddResult __ar__ = (_EXPR_); if(not __ar__.success) { return __ar__; } }

ClassDefinition::AddResult ClassDefinition::addField(const Type* _type, const QString& _name, Field::Tags _tags, const QString& _initialiser, const QStringList& _unserialiseWith)
{
  Field f = {_type, _name, _tags, _initialiser, _unserialiseWith};
  d->fields.append(f);
  if(f.tags.testFlag(Field::Tag::Key))
  {
    d->keyField = f;
  }
  if(f.tags.testFlag(Field::Tag::Unserialisable))
  {
    return cdSuccess();
  }
  // Check for serialisation arguments
  if(f.tags.testFlag(Field::Tag::Reference) or f.tags.testFlag(Field::Tag::External))
  {
    if(f.unserialiseWith.size() == 1)
    {
      QString name = f.unserialiseWith.first();
      
      const Type* t = nullptr;
      if(f.tags.testFlag(Field::Tag::Reference))
      {
        t = f.type->nested() ? f.type->nested()->setOf() : f.type->setOf();
      } else {
        t = f.type; // for external
      }
      RETURN_FAILURE(addUnserialiseArguments(name, t, true));
    } else {
      return cdFailure(QString("Wrong number of unserialisation reference in field '%1' should be one.").arg(f.name));
    }
  } else {
      const ClassDefinition* cd = nullptr;
      const TypeDefinition* td = nullptr;
      switch(f.type->type())
      {
        case Type::TType::BuiltIn:
        case Type::TType::Enum:
        case Type::TType::Flags:
          break;
        case Type::TType::Class:
          cd = dynamic_cast<const ClassDefinition*>(f.type);
          break;
        case Type::TType::Vector:
        case Type::TType::List:
        case Type::TType::Set:
          cd = dynamic_cast<const ClassDefinition*>(f.type->nested());
          break;
        case Type::TType::TypeDef:
          td = dynamic_cast<const TypeDefinition*>(f.type);
          break;
      }
      if(not (cd or td))
      {
        return (f.unserialiseWith.size() > 0) ? cdFailure(QString("Type '%1' is not a class definition or type definition").arg(f.type->name())) : cdSuccess();
      }
      if(cd)
      {
        QList<UnserialisationArgument> f_uA;
        for(const ClassDefinition::UnserialisationArgument& p : cd->unserialisationArguments())
        {
          if(not p.name.endsWith("()") and not p.name.contains("."))
          {
            f_uA.append(p);
          }
        }
        if(f_uA.size() != f.unserialiseWith.size())
        {
          return cdFailure(QString("Invalid number of serialisation arguments expected '%1' for class '%2' got '%3'").arg(f_uA.size()).arg(cd->name()).arg(f.unserialiseWith.size()));
        }
        for(int i = 0; i< f_uA.size(); ++i)
        {
          QString name = f.unserialiseWith[i];
          RETURN_FAILURE(addUnserialiseArguments(name, f_uA[i].type, true));
        }
      } else {
        Q_ASSERT(td);
        QList<TypeDefinition::UnserialisationArgument> f_uA = td->unserialisationArguments();
        if(f_uA.size() != f.unserialiseWith.size())
        {
          return cdFailure(QString("Invalid number of serialisation arguments expected '%1' got '%2'").arg(f_uA.size()).arg(f.unserialiseWith.size()));
        }
        for(int i = 0; i< f_uA.size(); ++i)
        {
          QString name = f.unserialiseWith[i];
          RETURN_FAILURE(addUnserialiseArguments(name, f_uA[i].type, true));
        }
      }
      
  }
  
  return cdSuccess();
}

QList<ClassDefinition::Function> ClassDefinition::pureVirtualFunctions() const
{
  QList<Function> pv_functions;
  for(const Function& f : functions())
  {
    if(f.tags.testFlag(Function::Tag::PureVirtual))
    {
      pv_functions.append(f);
    }
  }
  if(parent())
  {
    for(const Function& ppv : parent()->pureVirtualFunctions())
    {
      bool overriden = false;
      for(const Function& f : functions())
      {
        if(f.tags.testFlag(Function::Tag::Override) and f.name == ppv.name and f.arguments == ppv.arguments)
        {
          overriden = true;
          break;
        }
      }
      if(not overriden)
      {
        pv_functions.append(ppv);
      }
    }
  }
  return pv_functions;
}

ClassDefinition::AddResult ClassDefinition::addFunction(const Type* _returnType, const QString& _name, const QList<Function::Argument>& _arguments, Function::Access _access, Function::Tags _tags)
{
  d->calls.append({_returnType, _name, _arguments, _access, _tags});
  return cdSuccess();
}

const Enum* ClassDefinition::Private::addEnum(const QString& _name, ClassDefinition* _self, const QStringList& _values, bool _is_flag)
{

  Enum* e = new Enum(_name, _self, _values, _is_flag);
  enums.append(e);
  typesManager->addType(e);
  return e;
}

ClassDefinition::AddResult ClassDefinition::addEnum(const QString& _name, const QStringList& _values, bool _is_flag)
{
  if(d->addEnum(_name, this, _values, _is_flag))
  {
    return cdSuccess();
  } else {
    return cdFailure("Failed to create enum");
  }
}

ClassDefinition::AddResult ClassDefinition::addFlags(const QString& _enum_name, const QString& _flags_name, const QStringList& _values)
{
  const Enum* e = d->addEnum(_enum_name, this, _values, true);
  if(e)
  {
    Flags* f = new Flags(_flags_name, e, this);
    d->flags.append(f);
    d->typesManager->addType(f);
    return cdSuccess();
  } else {
    return cdFailure("Failed to create enum");
  }
}

ClassDefinition::AddResult ClassDefinition::addUnserialiseArguments(const QString& _name, const Type* _type, bool _required)
{
  if(not _type)
  {
    return cdFailure(QString("Cannot use '%1' in unserialisation").arg(_name));
  }
  if(hasField(_name))
  {
    return cdSuccess();
  }
  if(d->parent)
  {
    bool has_parent_match = false;
    for(const UnserialisationArgument& parent_ua : d->parent->unserialisationArguments())
    {
      if(parent_ua.name == _name and parent_ua.type == _type)
      {
        has_parent_match = true;
        break;
      }
    }
    if(not has_parent_match)
    {
      return cdFailure(QString("'%1' with type '%2' is not an unserilisation argument of parent class").arg(_name).arg(_type->name()));
    }
  }
  for(UnserialisationArgument& p : d->unserialisationArguments)
  {
    if(p.name == _name)
    {
      if(p.type == _type)
      {
        p.required = p.required or _required;
        return cdSuccess();
      } else {
        return cdFailure(QString("Conflicting type for '%1'").arg(_name));
      }
    }
  }
  d->unserialisationArguments.append({_name, _type, _required});
  return cdSuccess();
}

QList<ClassDefinition::UnserialisationArgument> ClassDefinition::unserialisationArguments() const
{
  return d->unserialisationArguments;
}
