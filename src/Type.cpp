#include "Type.h"

#include <QList>
#include <QString>

#include "ClassDefinition.h"

struct Type::Private
{
  Private() : listOf(0) {}
  TType type;
  QString name, namespace_, builtInName, builtInArgumentName, header;
  mutable const Type* listOf  = nullptr;
  mutable const Type* vectorOf  = nullptr;
  mutable const Type* setOf   = nullptr;
  const Type* nested          = nullptr;
};

Type::Type(const QString& _name, const QString& _namespace, TType _type) : d(new Private)
{
  Q_ASSERT(_type == TType::Class or _type == TType::Enum or _type == TType::Flags);
  d->type     = _type;
  d->name     = _name;
  d->namespace_ = _namespace;
}

Type::Type(const QString& _name, const QString& _cppname, const QString& _cppArgumentName, const QString& _header, TType _type) : d(new Private)
{
  d->type       = _type;
  d->name       = _name;
  d->builtInName          = _cppname;
  d->builtInArgumentName  = _cppArgumentName;
  d->header     = _header;
}

Type::Type(const Type* _nested, TType _type) : d(new Private)
{
  Q_ASSERT(_type == TType::Set or _type == TType::List or _type == TType::Vector);
  d->type     = _type;
  d->name     = (_type == TType::List ? "list of " : (_type == TType::Vector ? "vector of " : "set of ")) + _nested->name();
  d->nested   = _nested;
}

Type::~Type()
{
  delete d->listOf;
  delete d;
}

QString Type::name() const
{
  return d->name;
}

QString Type::namespaces() const
{
  return d->namespace_;
}

QString Type::builtInName() const
{
  return d->builtInName;
}

QString Type::builtInArgumentName() const
{
  return d->builtInArgumentName;
}

QString Type::header() const
{
  return d->header;
}

Type::TType Type::type() const
{
  return d->type;
}

const Type* Type::listOf() const
{
  if(d->listOf == 0)
  {
    d->listOf = new Type(this, TType::List);
  }
  return d->listOf;
}

const Type* Type::vectorOf() const
{
  if(d->vectorOf == 0)
  {
    d->vectorOf = new Type(this, TType::Vector);
  }
  return d->vectorOf;
}

const Type* Type::setOf() const
{
  if(d->setOf == nullptr)
  {
    if(d->type == TType::Class and static_cast<const ClassDefinition*>(this)->hasKey())
    {
      d->setOf = new Type(this, TType::Set);
    }
  }
  return d->setOf;
}

const Type* Type::nested() const
{
  return d->nested;
}
