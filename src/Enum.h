#include "Type.h"

class ClassDefinition;

class Enum : public Type
{
public:
  Enum(const QString& _name, const ClassDefinition* _parent, const QStringList& _values, bool _isFlag);
  ~Enum();
  const ClassDefinition* parent() const;
  QStringList values() const;
  bool isFlag() const;
private:
  struct Private;
  Private* const d;
};
