#include "Generator.h"

#include <QDir>
#include <QFile>
#include <QTextStream>

#include "ClassDefinition.h"
#include "Enum.h"
#include "Flags.h"
#include "TypesManager.h"

Generator::Generator()
{

}

Generator::~Generator()
{

}

namespace
{
  QString trueOrFalse(bool _v)
  {
    return _v ? QStringLiteral("true") : QStringLiteral("false");
  }
  void fillHeaders(QStringList* _headers, const ClassDefinition* _cd)
  {
    for(const ClassDefinition::Field& f : _cd->fields())
    {
      QString h = f.type->header();
      if(not h.isEmpty() and not _headers->contains(h)) _headers->append(h);
    }
    for(const ClassDefinition::Function& f : _cd->functions())
    {
      QString h = f.returnType->header();
      if(not h.isEmpty() and not _headers->contains(h)) _headers->append(h);
      for(const ClassDefinition::Function::Argument& a : f.arguments)
      {
        QString h = a.type->header();
        if(not h.isEmpty() and not _headers->contains(h)) _headers->append(h);
      }
    }
  }
  QString upcase(QString _name)
  {
    _name[0] = _name[0].toUpper();
    return _name;
  }
  QString typenameMember(const Type* _type, const ClassDefinition* _context);
  QString typenameResolved(const Type* _type, const ClassDefinition* _context)
  {
    switch(_type->type())
    {
      case Type::TType::Enum:
      {
        const Enum* e = static_cast<const Enum*>(_type);
        if(e->parent() != _context)
        {
          return typenameResolved(e->parent(), nullptr) + "::" + _type->name();
        } else {
          return _type->name();
        }
      }
      case Type::TType::Flags:
      {
        const Flags* f = static_cast<const Flags*>(_type);
        if(f->parent() != _context)
        {
          return typenameResolved(f->parent(), nullptr) + "::" + _type->name();
        } else {
          return _type->name();
        }
      }
      case Type::TType::Class:
      {
        QString nss = _type->namespaces().isEmpty() ? QString() : ("::" + _type->namespaces() + "::");
        return QString("%1%2").arg(nss).arg(_type->name());
      }
      case Type::TType::TypeDef:
      case Type::TType::BuiltIn:
      case Type::TType::Vector:
      case Type::TType::List:
      case Type::TType::Set:
        return typenameMember(_type, _context);
    }
    qFatal("impossible");
  }
  QString typenameListElement(const Type* _type, const ClassDefinition* _context)
  {
    switch(_type->type())
    {
      case Type::TType::TypeDef:
      case Type::TType::BuiltIn:
        return typenameMember(_type, _context);
      case Type::TType::Enum:
      case Type::TType::Flags:
        return typenameResolved(_type, _context);
      case Type::TType::Class:
        return QString("%1*").arg(typenameResolved(_type, _context));
      case Type::TType::Vector:
        return "QVector<" + typenameListElement(_type->nested(), _context) + ">";
      case Type::TType::List:
        return "QList<" + typenameListElement(_type->nested(), _context) + ">";
      case Type::TType::Set:
        return "ag::Set<" + typenameListElement(_type->nested(), _context) + ">";
    }
    qFatal("impossible");
  }
  /**
   * @return the typename for a member of a class
   */
  QString typenameMember(const Type* _type, const ClassDefinition* _context)
  {
    switch(_type->type())
    {
      case Type::TType::Enum:
      case Type::TType::Flags:
        return typenameResolved(_type, _context);
      case Type::TType::BuiltIn:
      case Type::TType::TypeDef:
        return _type->builtInName();
      case Type::TType::Class:
        return QString("%1*").arg(typenameResolved(_type, _context));
      case Type::TType::Vector:
        return "QVector<" + typenameListElement(_type->nested(), _context) + ">";
      case Type::TType::List:
        return "QList<" + typenameListElement(_type->nested(), _context) + ">";
      case Type::TType::Set:
        return "ag::Set<" + typenameListElement(_type->nested(), _context) + ">";
    }
    qFatal("impossible");
  }
  /**
   * @return the typename for a Q_PROPERTY definition
   */
  QString typenameProperty(const Type* _type, const ClassDefinition* _context)
  {
    switch(_type->type())
    {
      case Type::TType::TypeDef:
      case Type::TType::Enum:
      case Type::TType::Flags:
      case Type::TType::BuiltIn:
      case Type::TType::Class:
        return typenameMember(_type, _context);
      case Type::TType::Vector:
        if(_type->nested()->type() == Type::TType::Class)
        {
          return "QVector<QObject*>";
        } else {
          return "QVector<" + typenameResolved(_type->nested(), _context) + ">";
        }
      case Type::TType::List:
        if(_type->nested()->type() == Type::TType::Class)
        {
          return "QList<QObject*>";
        } else {
          return "QList<" + typenameResolved(_type->nested(), _context) + ">";
        }
      case Type::TType::Set:
        return "QVariant";
    }
    qFatal("impossible");
  }
  QString typenameArgument(const Type* _type, const ClassDefinition* _context)
  {
    switch(_type->type())
    {
      case Type::TType::Enum:
      case Type::TType::Flags:
        return typenameResolved(_type, _context);
      case Type::TType::TypeDef:
      {
        QString r = _type->builtInArgumentName();
        if(r[r.size() - 1] == '*')
        {
          return r;
        } else {
          return "const " + r + "&";
        }
      }
      case Type::TType::BuiltIn:
        return _type->builtInArgumentName();
      case Type::TType::Class:
        return QString("%1*").arg(typenameResolved(_type, _context));
      case Type::TType::Vector:
        return "const QVector<" + typenameListElement(_type->nested(), _context) + ">&";
      case Type::TType::List:
        return "const QList<" + typenameListElement(_type->nested(), _context) + ">&";
      case Type::TType::Set:
        return "const ag::Set<" + typenameListElement(_type->nested(), _context) + ">&";
    }
    qFatal("impossible");
  }
  QString typenameClassGetterReturn(const Type* _type, const ClassDefinition* _context)
  {
    switch(_type->type())
    {
      case Type::TType::Enum:
      case Type::TType::Flags:
        return typenameResolved(_type, _context);
      case Type::TType::Class:
        return QString("%1*").arg(typenameResolved(_type, _context));
      case Type::TType::TypeDef:
      case Type::TType::BuiltIn:
      case Type::TType::Vector:
      case Type::TType::List:
      case Type::TType::Set:
        return typenameResolved(_type, _context);
    }
    qFatal("impossible");
  }
  bool shouldHaveQListQObject(const ClassDefinition::Field& _field)
  {
    return _field.type->type() == Type::TType::List and _field.type->nested()->type() == Type::TType::Class;
  }
  bool shouldHaveQVectorQObject(const ClassDefinition::Field& _field)
  {
    return _field.type->type() == Type::TType::Vector and _field.type->nested()->type() == Type::TType::Class;
  }
  bool shouldHaveQVariantHash(const ClassDefinition::Field& _field)
  {
    return _field.type->type() == Type::TType::Set;
  }
  QString propertyReader(const ClassDefinition::Field& _field)
  {
    if(shouldHaveQListQObject(_field))
    {
      return _field.name + "AsQListQObject";
    } else if(shouldHaveQVectorQObject(_field))
    {
      return _field.name + "AsQVectorQObject";
    } else if(shouldHaveQVariantHash(_field))
    {
      return _field.name + "AsQVariant";
    } else {
      return _field.name;
    }
  }
  bool hasSetter(const ClassDefinition::Field& field)
  {
    return (field.type->type() != Type::TType::Class and not field.tags.testFlag(ClassDefinition::Field::Tag::Const)) or field.tags.testFlag(ClassDefinition::Field::Tag::External) or field.tags.testFlag(ClassDefinition::Field::Tag::Reference);
  }
  bool hasBuilder(const ClassDefinition::Field& field)
  {
    return field.type->type() == Type::TType::Class;
  }
  bool hasAppend(const ClassDefinition::Field& field)
  {
    return (field.type->type() == Type::TType::Set or field.type->type() == Type::TType::List);
  }
  
  bool implementsLoadable(const ClassDefinition* _definition)
  {
    return _definition->interfaces().contains("loadable");
  }
  bool implementsAutoload(const ClassDefinition* _definition)
  {
    return _definition->interfaces().contains("autoload");
  }
  QString parentClass(const ClassDefinition* _definition)
  {
    return _definition->parent() ? typenameResolved(_definition->parent(), nullptr) : "QObject";
  }
  bool hasProtectedInitialiser(const ClassDefinition* _definition)
  {
    if(_definition->parent() and hasProtectedInitialiser(_definition->parent())) return true;
    
    for(const ClassDefinition::Field& field : _definition->fields())
    {
      if(field.tags.testFlag(ClassDefinition::Field::Tag::Const))
      {
        return true;
      }
    }
    return false;
  }
  QList<ClassDefinition::Field> allFields(const ClassDefinition* _definition)
  {
    QList<ClassDefinition::Field> fields = _definition->fields();
    if(_definition->parent())
    {
      fields += allFields(_definition->parent());
    }
    return fields;
  }
  QList<ClassDefinition::Field> protectedInitialiserFields(const ClassDefinition* _definition)
  {
    QList<ClassDefinition::Field> fields;
    for(const ClassDefinition::Field& f : _definition->fields())
    {
      if(f.tags.testFlag(ClassDefinition::Field::Tag::Const))
      {
        fields.append(f);
      }
    }
    if(_definition->parent())
    {
      fields += protectedInitialiserFields(_definition->parent());
    }
    return fields;
  }
  QString protectedInitialiserArguments(const ClassDefinition* _definition)
  {
    QList<ClassDefinition::Field> fields = protectedInitialiserFields(_definition);
    QString arguments;
    for(const ClassDefinition::Field& f : fields)
    {
      arguments += typenameArgument(f.type, _definition) + " _" + f.name + ", ";
    }
    return arguments;
  }
  QString accessString(ClassDefinition::Function::Access _access)
  {
    switch(_access)
    {
      case ClassDefinition::Function::Access::Private:
        return QStringLiteral("private");
      case ClassDefinition::Function::Access::Protected:
        return QStringLiteral("protected");
      case ClassDefinition::Function::Access::Public:
        return QStringLiteral("public");
    }
    qFatal("impossible error");
  }
  
  QString argumentList(const ClassDefinition* _context, const QList<ClassDefinition::UnserialisationArgument>& _arguments, bool _start_with_comma, bool _end_with_comma, bool _only_required)
  {
    QStringList list;
    for(const ClassDefinition::UnserialisationArgument& p : _arguments)
    {
      if((not _only_required or p.required) and not p.name.endsWith("()") and not p.name.contains("."))
      {
        list.append(QString("%1 %2").arg(typenameArgument(p.type, _context)).arg(p.name));
      }
    }
    
    if(list.isEmpty()) return QString();
    return (_start_with_comma ? ", " : "") + list.join(", ") + (_end_with_comma ? ", " : "");
  }
  QString argumentNameList(const QList<ClassDefinition::UnserialisationArgument>& _arguments, bool _start_with_comma, bool _only_required)
  {
    QStringList list;
    for(const ClassDefinition::UnserialisationArgument& p : _arguments)
    {
      if((not _only_required or p.required) and not p.name.endsWith("()") and not p.name.contains("."))
      {
        list.append(p.name);
      }
    }
    
    if(list.isEmpty()) return QString();
    return (_start_with_comma ? ", " : "") + list.join(", ");
  }
  QString argumentNameList(const QStringList& _arguments, const ClassDefinition* _cd, bool _start_with_comma)
  {
    if(_arguments.isEmpty()) return QString();
    QStringList list;
    for(const QString& name : _arguments)
    {
      if(_cd->hasField(name))
      {
        list.append("_data_->" + name);
      } else {
        list.append(name);
      }
    }
    
    return (_start_with_comma ? ", " : "") + list.join(", ");
  }
  QStringList allEnumValues(const Enum* _e)
  {
    if(_e->isFlag())
    {
      return _e->values() + QStringList{"All", "None"};
    } else {
      return _e->values();
    }
  }
  QString fullClassName(const ClassDefinition* _klass)
  {
    if(_klass->namespaces().isEmpty())
    {
      return _klass->name();
    } else {
      return _klass->namespaces().join("::") + "::" + _klass->name();
    }
  }
  bool isReferenceOrExternal(const ClassDefinition::Field& field)
  {
    return field.tags.testFlag(ClassDefinition::Field::Tag::Reference) or field.tags.testFlag(ClassDefinition::Field::Tag::External);
  }
}

void Generator::generate(const TypesManager* _typesManager, const QList< ClassDefinition* > _definitions, const QString& _output, const QStringList& _importedHeaders)
{
  Q_UNUSED(_typesManager);
  QDir().mkpath(QFileInfo(_output).absolutePath());
  
  {
    QFile header_file(_output + ".h");
    header_file.open(QIODevice::WriteOnly);
    QTextStream stream(&header_file);
    
#include "Class_h.h"
  }
  {
    QFile body_file(_output + ".cpp");
    body_file.open(QIODevice::WriteOnly);
    QTextStream stream(&body_file);
#include "Class_cpp.h"
  }
}
