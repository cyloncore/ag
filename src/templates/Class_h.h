#pragma once

#include <ag/qt.h>

#include <QObject>

/%
for(const QString& h : _importedHeaders)
{
  %/
#include </%= h %/>/%
}
%/

/%

QStringList headers;

for(const ClassDefinition* cd : _definitions)
{
  fillHeaders(&headers, cd);
}

for(const QString& header : headers)
{
  %/
#include </%= header %/>/%
}

%/

/%

for(const ClassDefinition* cd : _definitions)
{
  if(not cd->namespaces().isEmpty())
  {
%/
namespace /%= cd->namespaces().join("::") %/
{/%
  }
  %/
  class /%= cd->name() %/ : public /%= parentClass(cd) %/
  {
    template<typename _T_, bool reference, typename _Enabled_ >
    friend struct ::ag::TypeSupport;
    Q_OBJECT/%
    if(implementsLoadable(cd))
    {%/
    Q_PROPERTY(QString filename READ filename WRITE setFilename NOTIFY filenameChanged)
    Q_PROPERTY(QString errorMessage READ errorMessage NOTIFY errorMessageChanged)/%
    }
    for(const ClassDefinition::Field& field : cd->fields())
    {
      if(not field.tags.testFlag(ClassDefinition::Field::Tag::Private) and not field.tags.testFlag(ClassDefinition::Field::Tag::Protected))
      {
      %/
      Q_PROPERTY(/%= typenameProperty(field.type, cd) %/ /%= field.name %/ READ /%= propertyReader(field) %/ /%
        if(hasSetter(field) and not field.tags.testFlag(ClassDefinition::Field::Tag::ReadOnly))
        {
          %/WRITE set/%= upcase(field.name) %/ /%
        }
      %/NOTIFY /%= field.name %/Changed)/%
      }
    }
    for(const Enum* e : cd->enums())
    {
      %/
  public:
    enum class /%= e->name() %/
    {
      /% if(e->isFlag())
      {%/
      None = 0,/%
      for(int i = 0; i < e->values().size(); ++i)
      {%/
      /%= e->values()[i] %/ = 1 << /%= i %/,/%
      }
      %/
      All = /%= e->values().join(" | ") %//%
      } else {%/
      /%= e->values().join(",\n      ") %/
      /%
      }%/
    };
    Q_ENUM(/%= e->name() %/)
      /%
    }
    for(const Flags* f : cd->flags())
    {%/
    Q_DECLARE_FLAGS(/%= f->name() %/, /%= f->enumType()->name() %/)
    Q_FLAGS(/%= f->name() %/)/%
    }
    %/
  public:/%
    if(cd->pureVirtualFunctions().isEmpty())
    {
    %/
    struct Builder
    {
      operator /%= cd->name() %/ *()
      {
        return m_data;
      }/%
    for(const ClassDefinition::Field& field : allFields(cd))
    {
      if(hasSetter(field) and not field.tags.testFlag(ClassDefinition::Field::Tag::Private) and not field.tags.testFlag(ClassDefinition::Field::Tag::Protected))
      {
    %/
      Builder set/%= upcase(field.name) %/(/%= typenameArgument(field.type, cd) %/ _value)
      {
        m_data->set/%= upcase(field.name) %/(_value);
        return *this;
      }/%
      } else if(hasBuilder(field)) {
    %/
      Builder set/%= upcase(field.name) %/(/%= typenameResolved(field.type, cd) %/::Builder&& _value)
      {
        m_data->set/%= upcase(field.name) %/Builder(std::move(_value));
        return *this;
      }/%
      }
    }
    %/
    private:
      friend class /%= cd->name() %/;
      Builder(/%= cd->name() %/ * _data) : m_data(_data) {}
      /%= cd->name() %/ * m_data;
    };/%
    }%/
  public:
    /%= cd->name() %/(QObject* _parent = nullptr);/%
      if(hasProtectedInitialiser(cd))
      {%/
  protected:
    /%= cd->name() %/(/%= protectedInitialiserArguments(cd) %/QObject* _parent);
  public:
    /%
      }
    %/
    ~/%= cd->name() %/();/%
    if(cd->pureVirtualFunctions().isEmpty())
    {
    %/
    static Builder create()
    {
      return {new /%= cd->name() %/()};
    }
    Q_INVOKABLE/% if(not cd->parent()) { %/ virtual/%} %/ /%= typenameResolved(cd->cloneType(), cd) %/ * clone() const/% if(cd->parent()) { %/ override/%} %/;/%
    } else if(not cd->parent()) {
      %/
    Q_INVOKABLE virtual /%= typenameResolved(cd->cloneType(), cd) %/ * clone() const = 0;/%
    }%/
  protected:
    void fillClone(/%= cd->name() %/ * _destination) const;/%
    if(implementsLoadable(cd))
    {%/
  public:
    // Loadable interface
    Q_INVOKABLE bool load(/%= argumentList(cd, cd->unserialisationArguments(), false, true, true) %/QString* _errorMessage = 0);
    Q_INVOKABLE bool save();
    QString filename() const;
    void setFilename(const QString& _filename);
    QString errorMessage() const;
  signals:
    void filenameChanged();
    void errorMessageChanged();/%
      if(implementsAutoload(cd))
      {%/
    void autoloadFailed(const QString& errorMessage);
    void autoloadSucceeded();/%
      }
    }    
    //BEGIN fields getters and setters
    for(const ClassDefinition::Field& field : cd->fields())
    {
      if(field.tags.testFlag(ClassDefinition::Field::Tag::Private))
      {%/
  private:/%
      }
      else if(field.tags.testFlag(ClassDefinition::Field::Tag::Protected))
      {%/
  protected:/%
      }
      else
      {%/
  public:/%
      }
      %/
    /%= typenameClassGetterReturn(field.type, cd) %/ /%= field.name %/() const;/%
      if(hasSetter(field))
      {
      if(field.tags.testFlag(ClassDefinition::Field::Tag::ReadOnly))
      {%/
  protected:/%
      }%/
    void set/%= upcase(field.name) %/(/%= typenameArgument(field.type, cd) %/ _value);/%
        if(hasAppend(field))
        {%/
    void appendTo/%= upcase(field.name) %/(/%= typenameArgument(field.type->nested(), cd) %/ _value);/%
        }
      } else if(hasBuilder(field))
      {
        %/
  private:
    void set/%= upcase(field.name) %/Builder(/%= typenameResolved(field.type, cd) %/::Builder&& _value);
        /%
      }
    }
    //END fields getters and setters
    
    //BEGIN functions
    for(const ClassDefinition::Function& function : cd->functions())
    {
      %/
  /%= accessString(function.access) %/:
  /% if(function.tags.testFlag(ClassDefinition::Function::Tag::Invokable)) { %/ Q_INVOKABLE/% } if(function.tags.testFlag(ClassDefinition::Function::Tag::Virtual)) { %/ virtual/% } %/ /%= typenameClassGetterReturn(function.returnType, cd) %/ /%= function.name %/(/%
      for(int i = 0; i < function.arguments.size(); ++i)
      {
        if(i != 0) %/, /%
        %//%= typenameArgument(function.arguments[i].type, cd) %/ /%= function.arguments[i].name %//%
      }
      %/)/% if(function.tags.testFlag(ClassDefinition::Function::Tag::Constant)) { %/ const/% } if(function.tags.testFlag(ClassDefinition::Function::Tag::PureVirtual)) { %/ = 0/% } if(function.tags.testFlag(ClassDefinition::Function::Tag::Override)) { %/ override/% } %/;/%
    }
    //END functions
    %/
  private:/%
    for(const ClassDefinition::Field& field : cd->fields())
    {
      if(shouldHaveQListQObject(field))
      {
      %/
    QList<QObject*> /%= field.name %/AsQListQObject() const;
    void set/%= upcase(field.name) %/(const QList<QObject*>& _value);/%
      }
      if(shouldHaveQVectorQObject(field))
      {
      %/
    QVector<QObject*> /%= field.name %/AsQVectorQObject() const;
    void set/%= upcase(field.name) %/(const QVector<QObject*>& _value);/%
      }
      if(shouldHaveQVariantHash(field))
      {
      %/
    QVariant /%= field.name %/AsQVariant() const;
    void set/%= upcase(field.name) %/(const QVariant& _value);/%
      }
    }
    %/
  signals:
    void changed();/%
    for(const ClassDefinition::Field& field : cd->fields())
    {
      %/
    void /%= field.name %/Changed();/%
    }
    %/
  public:
    /// Compute the cryptographic hash for this object
    QByteArray hash(QCryptographicHash::Algorithm _algorithm) const;
    void hash(QCryptographicHash* _hash) const;
    // Serialisation
    /% if(not cd->parent()) { %/virtual /% } %/ QJsonObject toJson() const/% if(cd->parent()) { %/ override/% } %/;
    static /%= cd->name() %/ * fromJson(const QJsonObject& _object/%= argumentList(cd, cd->unserialisationArguments(), true, false, false) %/, QString* _errorMessage = nullptr);
    /% if(not cd->parent()) { %/virtual /% } %/ QCborMap toCbor() const/% if(cd->parent()) { %/ override/% } %/;
    static /%= cd->name() %/ * fromCbor(const QCborMap& _object/%= argumentList(cd, cd->unserialisationArguments(), true, false, false) %/, QString* _errorMessage = nullptr);
  protected:
    bool loadFromJson(const QJsonObject& _object/%= argumentList(cd, cd->unserialisationArguments(), true, false, true) %/, QString* _errorMessage);
    bool loadFromCbor(const QCborMap& _object/%= argumentList(cd, cd->unserialisationArguments(), true, false, true) %/, QString* _errorMessage);
    class __ag_Factory__ /% if(cd->parent()) { %/ : public /%= parentClass(cd) %/::__ag_Factory__ /% } %/
    {
    public:
      virtual ~__ag_Factory__();
      virtual /%= cd->name() %/ * fromJson(const QJsonObject& _object/%= argumentList(cd, cd->unserialisationArguments(), true, false, false) %/, QString* _errorMessage = nullptr) = 0;
      virtual /%= cd->name() %/ * fromCbor(const QCborMap& _object/%= argumentList(cd, cd->unserialisationArguments(), true, false, false) %/, QString* _errorMessage = nullptr) = 0;
    };
    static void registerFactory(const QString& _name, __ag_Factory__* _factory);
    static void unregisterFactory(const QString& _name);
    /%
    if(cd->parent())
    {%/
  private:
    struct __ag_FactoryImplementation__;
    struct __ag_Registration__;
    static __ag_Registration__ __ag_staticRegistration__;
    /%
    }
    %/
  private:
    struct __ag_Data__;
    __ag_Data__* const _data_;
  };/%
  if(cd->hasKey())
  {
    %/
  inline /%= typenameClassGetterReturn(cd->keyField().type, cd) %/ agKey(const /%= cd->name() %/ * _value)
  {
    return _value->/%= cd->keyField().name %/();
  }/%
  }
  if(not cd->namespaces().isEmpty())
  {
%/
}/%
  }  
%/

namespace ag
{
  template<>
  struct ClassTypeSupport<::/%= fullClassName(cd) %/ *> : std::true_type {};
/%
  for(const Enum* e : cd->enums())
  {%/
  template<>
  QString serialise<QString, ::/%= fullClassName(cd) %/::/%= e->name() %/>(::/%= fullClassName(cd) %/::/%= e->name() %/ _value);
  template<>
  bool unserialise<::/%= fullClassName(cd) %/::/%= e->name() %/>(const QString& _string, ::/%= fullClassName(cd) %/::/%= e->name() %/ * _value);
  template<>
  struct StringTypeSupport<::/%= fullClassName(cd) %/::/%= e->name() %/> : std::true_type {};/%
  }
  for(const Flags* f : cd->flags())
  {%/
  template<>
  QStringList serialise<QStringList, ::/%= fullClassName(cd) %/::/%= f->name() %/>(::/%= fullClassName(cd) %/::/%= f->name() %/ _value);
  template<>
  bool unserialise<::/%= fullClassName(cd) %/::/%= f->name() %/>(const QStringList& _string, ::/%= fullClassName(cd) %/::/%= f->name() %/ * _value);
  template<>
  struct StringListTypeSupport<::/%= fullClassName(cd) %/::/%= f->name() %/> : std::true_type {};/%
  }%/

}

Q_DECLARE_METATYPE(::/%= fullClassName(cd) %/ *)

/%
}
%/
