#pragma once

#include <QString>

class ClassDefinition;
class Enum;
class Flags;
class TypeDefinition;
class TypesManager;

class Type
{
  friend class ClassDefinition;
  friend class Enum;
  friend class Flags;
  friend class TypeDefinition;
  friend class TypesManager;
  friend class Parser;
public:
  enum class TType
  {
    BuiltIn, Class, List, Enum, Set, Flags, TypeDef, Vector
  };
private:
  Type(const QString& _name, const QString& _namespaces, TType _type);
  Type(const QString& _name, const QString& _cppName, const QString& _cppArgumentName, const QString& _header, TType _type);
public:
  Type(const Type* _nested, TType _type);
  virtual ~Type();
  TType type() const;
  QString name() const;
  QString namespaces() const;
  QString builtInName() const;
  QString builtInArgumentName() const;
  QString header() const;
  const Type* listOf() const;
  const Type* vectorOf() const;
  const Type* setOf() const;
  const Type* nested() const;
private:
  struct Private;
  Private* const d;
};
