
namespace test_private
{
  
  class CustomTest
  {
  };
}

namespace ag
{
  template<bool _Reference_>
  struct TypeSupport<::test_private::CustomTest*, _Reference_>
  {
    static void hash(QCryptographicHash*, const ::test_private::CustomTest* )
    {
    }
    static ::test_private::CustomTest* init()
    {
      return new ::test_private::CustomTest();
    }
    static void clean(::test_private::CustomTest** _t)
    {
      delete *_t;
      *_t = nullptr;
    }
    static ::test_private::CustomTest* clone(const ::test_private::CustomTest* )
    {
      return new ::test_private::CustomTest();
    }
  };
  template<bool _Reference_>
  struct SerialisationSupport<::test_private::CustomTest*, _Reference_>
  {

    static QJsonValue toJson(const ::test_private::CustomTest* )
    {
      return QJsonValue();
    }
    static bool fromJson(::test_private::CustomTest**, const QJsonValue&, QString* )
    {
      return true;
    }
    static QCborValue toCbor(const ::test_private::CustomTest* )
    {
      return QCborValue();
    }
    static bool fromCbor(::test_private::CustomTest**, const QCborValue&, QString* )
    {
      return true;
    }
  };
  template<bool _Reference_>
  struct TypeSupport<::test_private::CustomTest, _Reference_>
  {
    static void hash(QCryptographicHash*, const ::test_private::CustomTest )
    {
    }
    static ::test_private::CustomTest init()
    {
      return ::test_private::CustomTest();
    }
    static void clean(::test_private::CustomTest*)
    {
    }
    static ::test_private::CustomTest clone(const ::test_private::CustomTest& _ct)
    {
      return _ct;
    }
  };
  template<bool _Reference_>
  struct SerialisationSupport<::test_private::CustomTest, _Reference_>
  {

    static QJsonValue toJson(const ::test_private::CustomTest& )
    {
      return QJsonValue();
    }
    static bool fromJson(::test_private::CustomTest*, const QJsonValue&, QString* )
    {
      return true;
    }
    static QCborValue toCbor(const ::test_private::CustomTest& )
    {
      return QCborValue();
    }
    static bool fromCbor(::test_private::CustomTest*, const QCborValue&, QString* )
    {
      return true;
    }
  };
}
