/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Parser.h"

#include <QDebug>
#include <QFile>
#include <QDir>
#include <QList>
#include <QPair>

#include "Lexer.h"
#include "Error.h"
#include "TypeDefinition.h"
#include "TypesManager.h"

struct Parser::ParseContext
{
  TypesManager* root_manager;
  QList<Error> errors;
  QHash<QString, TypesManager*> namespace2manager;
  QStringList imported;
};

struct Parser::Private
{
  Lexer* lexer;
  Token tok;
  QString fileName;
  QList<ClassDefinition*> definitions;
  ParseContext* context;
  bool ownParseContext;
  QStringList includeDirs;
  QStringList headers;
};

namespace
{
  inline QString fullname(const ClassDefinition* _cd)
  {
    if(_cd->namespaces().isEmpty()) return _cd->name();
    else return _cd->Type::namespaces() + "::" + _cd->name();
  }
}

Parser::Parser(Lexer* _lexer, TypesManager* _manager, const QString& _filename, const QStringList& _includeDirs) : d(new Private)
{
  d->lexer                  = _lexer;
  d->fileName               = _filename;
  d->context                = new ParseContext;
  d->context->root_manager  = _manager;
  d->ownParseContext        = true;
  d->includeDirs            = _includeDirs;
}

Parser::Parser(Lexer* _lexer, Parser::ParseContext* _context, const QString& _filename, const QStringList& _includeDirs) : d(new Private)
{
  d->lexer                  = _lexer;
  d->fileName               = _filename;
  d->context                = _context;
  d->ownParseContext        = false;
  d->includeDirs            = _includeDirs;
}

Parser::~Parser()
{
  if(d->ownParseContext)
  {
    delete d->context;
  }
  delete d;
}

TypesManager* Parser::manager(const QStringList& _namespaces)
{
  if(_namespaces.isEmpty()) return d->context->root_manager;
  QString namespaces_key = _namespaces.join("::");
  if(d->context->namespace2manager.contains(namespaces_key))
  {
    return d->context->namespace2manager.value(namespaces_key);
  } else {
    QStringList namespaces_parent = _namespaces;
    namespaces_parent.removeLast();
    TypesManager* parent = manager(namespaces_parent);
    TypesManager* manager = new TypesManager(parent);
    d->context->namespace2manager[namespaces_key] = manager;
    return manager;
  }
}

std::tuple<QList<ClassDefinition*>, QStringList> Parser::parse()
{
  // Start Parsing
  getNextToken();
  if(d->tok.type == Token::END_OF_FILE)
  {
    reportUnexpected(d->tok);
  }
  while(d->tok.type != Token::END_OF_FILE and d->context->errors.empty())
  {
    switch(d->tok.type)
    {
      case Token::TYPEDEF:
      {
        parseTypedef(QStringList());
        isOfType(Token::SEMI);
        getNextToken();
        break;
      }
      case Token::NAMESPACE:
      {
        parseNamespaces(QStringList());
        break;
      }
      case Token::IMPORT:
      {
        parseImport();
        isOfType(Token::SEMI);
        getNextToken();
        break;
      }
      case Token::CLASS:
      {
        const ClassDefinition* t = parseClass(QStringList());
        if(t)
        {
          if(not d->context->root_manager->addType(t))
          {
            reportError(d->tok, QString("%1 is already defined").arg(t->name()));
            delete t;
            t = 0;
          }
          d->context->namespace2manager[fullname(t)] = const_cast<TypesManager*>(t->typesManager());
        }
        isOfType(Token::SEMI);
        getNextToken();
      }
        break;
      default:
      {
        reportUnexpected(d->tok);
        reachNext(Token::SEMI);
        getNextToken();
        break;
      }
    }
  }
  // Check if errors have occured
  if(not d->context->errors.empty())
  {
    qDeleteAll(d->definitions);
    d->definitions.clear();
  }
  return std::make_tuple(d->definitions, d->headers);
}

void Parser::parseImport()
{
  isOfType(Token::IMPORT);
  getNextToken();
  if(isOfType(Token::STRING))
  {
    QString agd_file = d->tok.string;
    getNextToken();
    if(isOfType(Token::STRING))
    {
      QString include_file = d->tok.string;
      getNextToken();
      d->headers.append(include_file);
      QString agd_fullfile;
      
      for(const QString& id : d->includeDirs)
      {
        QFileInfo fi(QDir(id).filePath(agd_file));
        if(fi.exists())
        {
          agd_fullfile = fi.absoluteFilePath();
        }
      }
      if(agd_fullfile.isEmpty())
      {
        reportError(d->tok, QString("Cannot import '%1', cannot be found in search path").arg(agd_file));
      } else
      {
        if(not d->context->imported.contains(agd_fullfile))
        {
          d->context->imported.append(agd_fullfile);
          QFile file(agd_fullfile);
          file.open(QIODevice::ReadOnly);
          if(file.isOpen())
          {
            Lexer lexer(&file);
            QStringList nincl = d->includeDirs;
            nincl.takeFirst();
            nincl.prepend(QFileInfo(agd_fullfile).absoluteDir().absolutePath());
            Parser parser(&lexer, d->context, agd_fullfile, nincl);
            parser.parse();
          } else {
            reportError(d->tok, QString("Cannot import '%1', file '%2' does not exists").arg(agd_file).arg(agd_fullfile));
          }
        }
      }
    }
  }
}

void Parser::parseTypedef(const QStringList& _namespaces)
{
  isOfType(Token::TYPEDEF);
  getNextToken();
  if(isOfType(Token::STRING))
  {
    QString cpp_type_name = d->tok.string;
    getNextToken();
    if(isOfType(Token::IDENTIFIER))
    {
      QString type_name = d->tok.string;
      QStringList namespaces = _namespaces;
      getNextToken();
      
      while(d->tok.type == Token::COLONCOLON)
      {
        namespaces.append(type_name);
        getNextToken();
        if(isOfType(Token::IDENTIFIER))
        {
          type_name = d->tok.string;
          getNextToken();
        } else {
          reachNext(Token::SEMI);
          return;
        }
      }
      
      QString header;
      if(d->tok.type == Token::FROM)
      {
        getNextToken();
        if(isOfType(Token::STRING))
        {
          header = d->tok.string;
          getNextToken();
        }
      }
      TypeDefinition* td = new TypeDefinition(type_name, cpp_type_name, cpp_type_name, header);
      manager(namespaces)->addType(td);
      if(d->tok.type == Token::UNSERIALISE)
      {
        getNextToken();
        if(isOfType(d->tok, Token::WITH))
        {
          getNextToken();
          if(isOfType(d->tok, Token::STARTBRACKET))
          {
            getNextToken();
            while(d->tok.type != Token::END_OF_FILE)
            {
              const Type* type = parseType(_namespaces, manager(_namespaces), true, nullptr);
              if(type)
              {
                if(isOfType(d->tok, Token::IDENTIFIER))
                {
                  td->addUnserialiseArguments(d->tok.string, type);
                  getNextToken();
                  if(d->tok.type == Token::ENDBRACKET)
                  {
                    getNextToken();
                    break;
                  } else if(d->tok.type == Token::COMMA)
                  {
                    getNextToken();
                  } else {
                    reportUnexpected(d->tok);
                    return;
                  }
                } else {
                  return;
                }
              } else {
                return;
              }
            }
          }
        }
      }
      return;
    }
  }
  reachNext(Token::SEMI);
}

void Parser::parseNamespaces(const QStringList& _namespaces)
{
  QStringList namespaces = _namespaces;
  isOfType(Token::NAMESPACE);
  getNextToken();
  while(d->tok.type != Token::END_OF_FILE)
  {
    if(isOfType(Token::IDENTIFIER))
    {
      namespaces.append(d->tok.string);
      getNextToken();
      if(d->tok.type != Token::COLONCOLON)
      {
        break;
      }
      getNextToken(); // eats '::'
    }
  }
  if(isOfType(Token::STARTBRACE))
  {
    getNextToken();
  }
  while(d->tok.type != Token::ENDBRACE)
  {
    switch(d->tok.type)
    {
      case Token::NAMESPACE:
      {
        parseNamespaces(namespaces);
        break;
      }
      case Token::CLASS:
      {
        const ClassDefinition* t = parseClass(namespaces);
        if(t)
        {
          if(manager(namespaces)->addType(t))
          {
            d->context->namespace2manager[fullname(t)] = const_cast<TypesManager*>(t->typesManager());
          }
          else
          {
            const ClassDefinition* pcd = dynamic_cast<const ClassDefinition*>(manager(namespaces)->type(t->name(), false));
            if(pcd and pcd->isForwardDefinition())
            {
              const_cast<ClassDefinition*>(pcd)->swap(const_cast<ClassDefinition*>(t));
            } else {
              reportError(d->tok, QString("%1 is already defined").arg(t->name()));
            }
            d->definitions.removeAll(const_cast<ClassDefinition*>(t));
            delete t;
            t = nullptr;
          }
        }
        isOfType(Token::SEMI);
        getNextToken();
      }
        break;
      case Token::END_OF_FILE:
        reportUnexpected(d->tok);
        return;
      default:
      {
        reportUnexpected(d->tok);
        reachNext(Token::SEMI);
        getNextToken();
        break;
      }
    }
  }
  getNextToken();
}

const ClassDefinition* Parser::parseClass(const QStringList& _namespaces)
{
  isOfType(Token::CLASS);
  getNextToken();
  if(isOfType(Token::IDENTIFIER))
  {
    QString className = d->tok.string;
    getNextToken();
    
    if(d->tok.type == Token::SEMI)
    {
      return new ClassDefinition(className, _namespaces, manager(_namespaces));
    }
    
    const ClassDefinition* parent = nullptr;
    
    // parse implementation
    QStringList interfaces;
    if(d->tok.type == Token::INHERITS)
    {
      getNextToken();
      if(isOfType(Token::IDENTIFIER))
      {
        parent = dynamic_cast<const ClassDefinition*>(parseType(_namespaces, manager(_namespaces), false, nullptr));
        if(not parent)
        {
          reportError(d->tok, QString("Unknown class '%1'").arg(d->tok.string));
          return nullptr;
        }
        interfaces = parent->interfaces();
      } else {
        return nullptr;
      }
    }
    if(d->tok.type == Token::IMPLEMENTS)
    {
      getNextToken();
      while(isOfType(Token::IDENTIFIER))
      {
        interfaces.append(d->tok.string);
        getNextToken();
        if(d->tok.type == Token::COMMA)
        {
          getNextToken();
        } else {
          break;
        }
      }
    }
    
    ClassDefinition* currentDefinition = new ClassDefinition(className, _namespaces, interfaces, parent, manager(_namespaces));
    if(isOfType(Token::STARTBRACE))
    {
      getNextToken();
      while(d->tok.type != Token::ENDBRACE and d->tok.type != Token::END_OF_FILE)
      {
        ClassDefinition::Field::Tags field_tags;
        bool parseTags = true;
        while(parseTags)
        {
          switch(d->tok.type)
          {
            case Token::CONST:
              field_tags.setFlag(ClassDefinition::Field::Tag::Const, true);
              getNextToken();
              break;
            case Token::READONLY:
              field_tags.setFlag(ClassDefinition::Field::Tag::ReadOnly, true);
              getNextToken();
              break;
            case Token::EXTERNAL:
              field_tags.setFlag(ClassDefinition::Field::Tag::External, true);
              getNextToken();
              break;
            case Token::KEY:
              field_tags.setFlag(ClassDefinition::Field::Tag::Key, true);
              getNextToken();
              break;
            case Token::REFERENCE:
              field_tags.setFlag(ClassDefinition::Field::Tag::Reference, true);
              getNextToken();
                break;
            case Token::UNSERIALISABLE:
              field_tags.setFlag(ClassDefinition::Field::Tag::Unserialisable, true);
              getNextToken();
              break;
            case Token::PRIVATE:
              field_tags.setFlag(ClassDefinition::Field::Tag::Private, true);
              getNextToken();
              break;
            case Token::PROTECTED:
              field_tags.setFlag(ClassDefinition::Field::Tag::Protected, true);
              getNextToken();
              break;
            default:
              parseTags = false;
          }
        }
        switch(d->tok.type)
        {
          case Token::ENUM:
          {
            getNextToken();
            if(isOfType(Token::IDENTIFIER))
            {
              Token enumTok = d->tok;
              getNextToken();
              if(isOfType(Token::STARTBRACE))
              {
                getNextToken();
              }
              QStringList values;
              while(d->tok.type != Token::ENDBRACE and d->tok.type != Token::END_OF_FILE)
              {
                isOfType(Token::IDENTIFIER);
                values.append(d->tok.string);
                getNextToken();
                if(d->tok.type == Token::COMMA)
                {
                  getNextToken();
                  if(not isOfType(Token::IDENTIFIER))
                  {
                    reachNext(Token::SEMI);
                    break;
                  }
                }
              }
              isOfType(Token::ENDBRACE);
              getNextToken();
              isOfType(Token::SEMI);
              if(values.isEmpty())
              {
                reportError(enumTok, "Empty enums are not allowed");
              } else {
                reportErrorOrNot(enumTok, currentDefinition->addEnum(enumTok.string, values, false));
              }
            } else {
              reachNext(Token::SEMI);
            }
            break;
          }
          case Token::FLAGS:
          {
            getNextToken();
            if(isOfType(Token::IDENTIFIER))
            {
              Token flagTok = d->tok;
              QString enumName;
              getNextToken();
              if(isOfType(Token::OF))
              {
                getNextToken();
              }
              if(isOfType(Token::IDENTIFIER))
              {
                enumName = d->tok.string;
                getNextToken();
              }
              if(isOfType(Token::STARTBRACE))
              {
                getNextToken();
              }
              QStringList values;
              while(d->tok.type != Token::ENDBRACE and d->tok.type != Token::END_OF_FILE)
              {
                isOfType(Token::IDENTIFIER);
                values.append(d->tok.string);
                getNextToken();
                if(d->tok.type == Token::COMMA)
                {
                  getNextToken();
                  if(not isOfType(Token::IDENTIFIER))
                  {
                    reachNext(Token::SEMI);
                    break;
                  }
                }
              }
              isOfType(Token::ENDBRACE);
              getNextToken();
              isOfType(Token::SEMI);
              if(values.isEmpty())
              {
                reportError(flagTok, "Empty flags are not allowed");
              } else {
                reportErrorOrNot(flagTok, currentDefinition->addFlags(enumName, flagTok.string, values));
              }
            } else {
              reachNext(Token::SEMI);
            }
            break;
          }
          case Token::IDENTIFIER:
          {
            Token tok = d->tok;
            getNextToken();
            if(d->tok.type == Token::EQUAL)
            {
              QString init = d->lexer->readInitialiser();
              reportErrorOrNot(tok, currentDefinition->addInitialiser(tok.string, init));
              getNextToken();
              isOfType(Token::SEMI);
              break;
            } else {
              d->lexer->unget(d->tok);
              d->tok = tok;
              Q_FALLTHROUGH();
            }
          }
          case Token::LIST:
          case Token::VECTOR:
          case Token::SET:
          {
            const Type* returnType = parseType(_namespaces, currentDefinition->typesManager(), true, currentDefinition);
            if(not returnType) return nullptr;
            if(isOfType(Token::IDENTIFIER))
            {
              QString funcname = d->tok.string;
              getNextToken();
              
              QString initialiser;
              QStringList unserialiseWith;
              
              switch(d->tok.type)
              {
                case Token::STARTBRACKET:
                {
                  getNextToken();
                  QList<ClassDefinition::Function::Argument> arguments; 
                  if(d->tok.type != Token::ENDBRACKET)
                  {
                    while(true)
                    {
                      const Type* argumentType = parseType(_namespaces, currentDefinition->typesManager(), true, currentDefinition);
                      if(argumentType)
                      {
                        ClassDefinition::Function::Argument argument;
                        argument.type = argumentType;
                        if(isOfType(Token::IDENTIFIER))
                        {
                          argument.name = d->tok.string;
                          arguments.append(argument);
                        }
                      }
                      getNextToken();
                      if(d->tok.type == Token::COMMA)
                      {
                        getNextToken();
                      } else if(d->tok.type == Token::ENDBRACKET) {
                        break;
                      } else {
                        reportUnexpected(d->tok);
                        return nullptr;
                      }
                    }
                  }
                  getNextToken(); // eat the ')'
                  ClassDefinition::Function::Tags tags;
                  if(d->tok.type == Token::CONST)
                  {
                    tags.setFlag(ClassDefinition::Function::Tag::Constant);
                    getNextToken();
                  }
                    ClassDefinition::Function::Access access = ClassDefinition::Function::Access::Public;
                    switch(d->tok.type)
                    {
                      case Token::PUBLIC:
                        getNextToken();
                        break;
                      case Token::PROTECTED:
                        access = ClassDefinition::Function::Access::Protected;
                        getNextToken();
                        break;
                      case Token::PRIVATE:
                        access = ClassDefinition::Function::Access::Private;
                        getNextToken();
                        break;
                      default:
                        break;
                    }
                    if(d->tok.type == Token::INVOKABLE)
                    {
                      tags.setFlag(ClassDefinition::Function::Tag::Invokable);
                      getNextToken();
                    }
                    if(d->tok.type == Token::VIRTUAL)
                    {
                      tags.setFlag(ClassDefinition::Function::Tag::Virtual);
                      getNextToken();
                    }
                    if(d->tok.type == Token::PUREVIRTUAL)
                    {
                      tags.setFlag(ClassDefinition::Function::Tag::Virtual);
                      tags.setFlag(ClassDefinition::Function::Tag::PureVirtual);
                      getNextToken();
                    }
                    if(d->tok.type == Token::OVERRIDE)
                    {
                      tags.setFlag(ClassDefinition::Function::Tag::Override);
                      getNextToken();
                    }
                    reportErrorOrNot(d->tok, currentDefinition->addFunction(returnType, funcname, arguments, access, tags));
                    if(not isOfType(Token::SEMI))
                    {
                      reachNext(Token::SEMI);
                    }
                }
                  break;
                case Token::EQUAL:
                  initialiser = d->lexer->readInitialiser();
                  getNextToken();
                  isOfType(Token::SEMI);
                  Q_FALLTHROUGH();
                case Token::UNSERIALISE:
                case Token::SEMI:
                  if(d->tok.type == Token::UNSERIALISE)
                  {
                    getNextToken();
                    isOfType(Token::WITH);
                    getNextToken();
                    isOfType(Token::STARTBRACKET);
                    getNextToken();
                    while(d->tok.type != Token::ENDBRACKET and d->tok.type != Token::END_OF_FILE)
                    {
                      isOfType(Token::IDENTIFIER);
                      QString unserialiseName = d->tok.string;
                      getNextToken();
                      while(d->tok.type == Token::STARTBRACKET
                        or d->tok.type == Token::DOT
                        or d->tok.type == Token::ARROW)
                      {
                        if(d->tok.type == Token::STARTBRACKET)
                        {
                          getNextToken();
                          isOfType(Token::ENDBRACKET);
                          getNextToken();
                          unserialiseName += "()";
                        }
                        if(d->tok.type == Token::DOT)
                        {
                          getNextToken();
                          isOfType(Token::IDENTIFIER);
                          unserialiseName += "." + d->tok.string;
                          getNextToken();
                        }
                        if(d->tok.type == Token::ARROW)
                        {
                          getNextToken();
                          isOfType(Token::IDENTIFIER);
                          unserialiseName += "->" + d->tok.string;
                          getNextToken();
                        }
                      }
                      unserialiseWith.append(unserialiseName);
                      switch(d->tok.type)
                      {
                        case Token::COMMA:
                          getNextToken();
                          break;
                        case Token::ENDBRACKET:
                          break;
                        default:
                          reportUnexpected(d->tok);
                          break;
                      }
                    }
                    getNextToken();
                  }
                  if(field_tags.testFlag(ClassDefinition::Field::Tag::Reference))
                  {
                    const Type* referenceType = returnType;
                    if(returnType->nested())
                    {
                      referenceType = referenceType->nested();
                    }
                    if(not referenceType->setOf())
                    {
                      reportError(d->tok, QString("%1 cannot be a reference").arg(referenceType->name()));
                    }
                    switch(unserialiseWith.size())
                    {
                      case 0:
                        reportError(d->tok, "Missing unserialiseWith statement.");
                        break;
                      case 1:
                        break;
                      default:
                        reportError(d->tok, "Too many identifier in unserialiseWith statement.");
                        break;
                    }
                  }
                  if(field_tags.testFlag(ClassDefinition::Field::Tag::Key) and currentDefinition->hasKey())
                  {
                    reportError(d->tok, QString("Only one key is accepted per class definition."));
                  }
                  if(field_tags.testFlag(ClassDefinition::Field::Tag::Key) and (returnType->name() != "string" and returnType->name() != "int" ))
                  {
                    reportError(d->tok, QString("Only string or int keys are supported."));
                  }
                  reportErrorOrNot(d->tok, currentDefinition->addField(returnType, funcname, field_tags, initialiser, unserialiseWith));
                  break;
                default:
                  reportUnexpected(d->tok);
              }
            }
          }
            break;
          case Token::UNSERIALISE:
          {
            getNextToken();
            if(isOfType(d->tok, Token::WITH))
            {
              getNextToken();
              if(isOfType(d->tok, Token::STARTBRACKET))
              {
                getNextToken();
                while(d->tok.type != Token::END_OF_FILE)
                {
                  const Type* t = parseType(_namespaces, currentDefinition->typesManager(), true, currentDefinition);
                  isOfType(d->tok, Token::IDENTIFIER);
                  if(t)
                  {
                    reportErrorOrNot(d->tok, currentDefinition->addUnserialiseArguments(d->tok.string, t, false));
                  }
                  getNextToken();
                  if(d->tok.type == Token::COMMA)
                  {
                    getNextToken();
                  } else if(d->tok.type == Token::ENDBRACKET)
                  {
                    getNextToken();
                    break;
                  } else {
                    reportUnexpected(d->tok);
                    reachNext(Token::SEMI);
                  }
                }
                isOfType(d->tok, Token::SEMI);
              }
            } else {
              reachNext(Token::SEMI);
            }
            break;
          }
          default:
            reportUnexpected(d->tok);
            reachNext(Token::SEMI);
            break;
        }
        getNextToken();
      }
    
      isOfType(Token::ENDBRACE);
      getNextToken();
      d->definitions.append(currentDefinition);
      return currentDefinition;
    }
  }
  reachNext(Token::SEMI);
  return nullptr;
}

#if 0
const Type* Parser::type(TypesManager* _currentManager, const QString& _name)
{
  const Type* t = _currentManager->type(_name);
  if(t)
  {
    return t;
  }
  if(d->currentDefinition)
  {
    t = d->currentDefinition->typesManager()->type(_name);
    if(t)
    {
      return t;
    }
  }
  reportError(d->tok, QString("Unknown type %1").arg(_name));
  return 0;
}
#endif

const Type* Parser::parseType(const QStringList& _namespaces, const TypesManager* _currentManager, bool _accept_coumpound, const ClassDefinition* _currentClass)
{
  bool is_list = false;
  bool is_set = false;
  bool is_vector = false;
  if(_accept_coumpound)
  {
    if(d->tok.type == Token::LIST)
    {
      is_list = true;
      getNextToken(); // eat 'list'
      if(isOfType(Token::OF))
      {
        getNextToken(); // eat 'of'
      }
    }
    if(d->tok.type == Token::VECTOR)
    {
      is_vector = true;
      getNextToken(); // eat 'vector'
      if(isOfType(Token::OF))
      {
        getNextToken(); // eat 'of'
      }
    }
    if(d->tok.type == Token::SET)
    {
      is_set = true;
      getNextToken(); // eat 'set'
      if(isOfType(Token::OF))
      {
        getNextToken(); // eat 'of'
      }
    }
  }
  
  if(isOfType(Token::IDENTIFIER))
  {
    QStringList namespaces;
    QString type_name = d->tok.string;
    getNextToken();
    while(d->tok.type == Token::COLONCOLON)
    {
      getNextToken();
      isOfType(Token::IDENTIFIER);
      namespaces.append(type_name);
      type_name = d->tok.string;
      getNextToken();
    }
    d->lexer->unget(d->tok);
    const Type* t = nullptr;
    if(namespaces.isEmpty())
    {
      t = _currentManager->type(type_name, true);
      if(t == nullptr and _currentClass != nullptr and _currentClass->name() == type_name)
      {
        t = _currentClass;
      }
    }
    QStringList active_namespaces = _namespaces;
    while(t == nullptr)
    {
      QString namespaces_name = (active_namespaces + namespaces).join("::");
      if(d->context->namespace2manager.contains(namespaces_name))
      {
        t = d->context->namespace2manager[namespaces_name]->type(type_name, false);
      }
      if(active_namespaces.isEmpty()) break;
      active_namespaces.takeLast();
    }
    if(t == nullptr)
    {
      reportError(d->tok, QString("Unknown type '%1'").arg((namespaces + QStringList{type_name}).join("::")));
      return nullptr;
    }
    getNextToken();
    if(is_list)
    {
      return t->listOf();
    } else if(is_vector)
    {
      return t->vectorOf();
    } else if(is_set)
    {
      const Type* ret = t->setOf();
      if(not ret)
      {
        reportError(d->tok, QString("Cannot use '%1' as set").arg(t->name()));
      }
      return ret;
    } else {
      return t;
    }
  }
  getNextToken();
  return 0;
}

void Parser::getNextToken()
{
  d->tok = d->lexer->nextToken();
}

bool Parser::isOfType(const Token& _token, Token::Type _type)
{
  if(_token.type == _type) return true;
  if(_token.string.isEmpty())
  {
    reportError(_token, QString("Expected token %1 got %2").arg(Token::typeToString(_type)).arg(Token::typeToString(_token.type)));
  } else {
    reportError(_token, QString("Expected token %1 got %2 (%3)").arg(Token::typeToString(_type)).arg(Token::typeToString(_token.type)).arg(_token.string));
  }
  return false;
}

bool Parser::isOfType(Token::Type _type)
{
  return isOfType(d->tok, _type);
}

void Parser::reachNext(Token::Type _type)
{
  while(d->tok.type != Token::END_OF_FILE and d->tok.type != _type)
  {
    getNextToken();
  }
}

QList< Error > Parser::errors() const
{
  return d->context->errors;
}

void Parser::reportErrorOrNot(const Token& _token, const ClassDefinition::AddResult& _add_result)
{
  if(not _add_result.success)
  {
    reportError(_token, _add_result.errorMessage);
  }
}

void Parser::reportError(const Token& _token, const QString& _errorMsg)
{
  Error err;
  err.line      = _token.line;
  err.column    = _token.column;
  err.filename  = d->fileName;
  err.message   = _errorMsg;
  d->context->errors.append(err);
}

void Parser::reportUnexpected(const Token& _token)
{
  if(_token.string.isEmpty())
  {
    reportError(_token, QString("Unexpected token %1").arg(Token::typeToString(_token.type)));
  } else {
    reportError(_token, QString("Unexpected token %1 (%2)").arg(Token::typeToString(_token.type)).arg(_token.string));
  }
}

void Parser::reportUnknownIndentifier(const Token& _token)
{
  Q_ASSERT(_token.type == Token::IDENTIFIER);
  reportError(_token, QString("Unknown identifier %1").arg(_token.string));
}
