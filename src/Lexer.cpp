/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Lexer.h"

#include <QDebug>
#include <QIODevice>

#include "Token.h"

struct Lexer::Private {
  QIODevice* device;
  QTextStream stream;
  QChar lastChar;
  int col;
  int line;
  int followingnewline;
  QList<Token> ungotten;
  QList<QChar> ungottenChar;
};

namespace
{
  bool isspace(QChar _c)
  {
    // qDebug() << _c << (_c.isSpace() or _c == '\n' or _c == '\r');
    return _c.isSpace() or _c == '\n' or _c == '\r';
  }
}

Lexer::Lexer(QIODevice* device) : d(new Private)
{
  d->device = device;
  d->stream.setDevice(device);
  d->col = 1;
  d->line = 1;
  d->followingnewline = 1;
}

Lexer::~Lexer()
{
  delete d;
}

#define IDENTIFIER_IS_KEYWORD( tokenname, tokenid) \
  if( identifierStr == tokenname) \
  { \
    return Token(Token::tokenid, line(), initial_col); \
  }

#define CHAR_IS_TOKEN( tokenchar, tokenid ) \
  if( lastChar == tokenchar ) \
  { \
    return Token(Token::tokenid, line(), initial_col); \
  }

#define CHAR_IS_TOKEN_OR_TOKEN( tokenchar, tokendecidechar, tokenid_1, tokenid_2 ) \
  if( lastChar == tokenchar  ) \
  { \
    if( getNextChar() == tokendecidechar ) \
    { \
      return Token(Token::tokenid_2, line(), initial_col); \
    } else { \
      unget(); \
      return Token(Token::tokenid_1, line(), initial_col); \
    } \
  }

#define CHAR_IS_TOKEN_OR_TOKEN_OR_TOKEN( tokenchar_1, tokenchar_2, tokenchar_3, tokenid_1, tokenid_2, tokenid_3 ) \
  if( lastChar == tokenchar_1 ) \
  { \
    int nextChar = getNextChar(); \
    if( nextChar == tokenchar_2 ) \
    { \
      return Token(Token::tokenid_2, line(), initial_col); \
    } else if( nextChar  == tokenchar_3 ) \
    { \
      return Token(Token::tokenid_3, line(), initial_col); \
    } else { \
      unget(); \
      return Token(Token::tokenid_1, line(), initial_col); \
    } \
  }

void Lexer::unget(const Token& _token)
{
  d->ungotten.append(_token);
}

Token Lexer::nextToken()
{
  if(not d->ungotten.isEmpty())
  {
    return d->ungotten.takeLast();
  }
  int initial_line = line() - 1;
  int initial_col = column() - 1;
  if( eof() ) return Token(Token::END_OF_FILE, line(), initial_col);
  QChar lastChar = getNextNonSeparatorChar();
  if(lastChar.isNull() and eof())
  {
    return Token(Token::END_OF_FILE, line(), initial_col);
  }
  QString identifierStr;
  // Test for comment
  Token commentToken;
  if( ignoreComment( commentToken, lastChar ) )
  {
    return commentToken;
  }
  // if it is alpha, it's an identifier or a keyword
  if(lastChar.isLetter() or lastChar == '_')
  {
    identifierStr = getIdentifier(lastChar);
    
    IDENTIFIER_IS_KEYWORD( "class", CLASS );
    IDENTIFIER_IS_KEYWORD( "namespace", NAMESPACE );
    IDENTIFIER_IS_KEYWORD( "list", LIST );
    IDENTIFIER_IS_KEYWORD( "of", OF );
    IDENTIFIER_IS_KEYWORD( "public", PUBLIC );
    IDENTIFIER_IS_KEYWORD( "private", PRIVATE );
    IDENTIFIER_IS_KEYWORD( "protected", PROTECTED );
    IDENTIFIER_IS_KEYWORD( "invokable", INVOKABLE );
    IDENTIFIER_IS_KEYWORD( "enum", ENUM );
    IDENTIFIER_IS_KEYWORD( "implements", IMPLEMENTS );
    IDENTIFIER_IS_KEYWORD( "inherits", INHERITS );
    IDENTIFIER_IS_KEYWORD( "const", CONST );
    IDENTIFIER_IS_KEYWORD( "readonly", READONLY );
    IDENTIFIER_IS_KEYWORD( "unserialisable", UNSERIALISABLE );
    IDENTIFIER_IS_KEYWORD( "typedef", TYPEDEF );
    IDENTIFIER_IS_KEYWORD( "from", FROM );
    IDENTIFIER_IS_KEYWORD( "key", KEY );
    IDENTIFIER_IS_KEYWORD( "reference", REFERENCE );
    IDENTIFIER_IS_KEYWORD( "with", WITH );
    IDENTIFIER_IS_KEYWORD( "unserialise", UNSERIALISE );
    IDENTIFIER_IS_KEYWORD( "set", SET );
    IDENTIFIER_IS_KEYWORD( "flags", FLAGS );
    IDENTIFIER_IS_KEYWORD( "import", IMPORT );
    IDENTIFIER_IS_KEYWORD( "optional", OPTIONAL );
    IDENTIFIER_IS_KEYWORD( "external", EXTERNAL );
    IDENTIFIER_IS_KEYWORD( "vector", VECTOR );
    IDENTIFIER_IS_KEYWORD( "virtual", VIRTUAL );
    IDENTIFIER_IS_KEYWORD( "pure_virtual", PUREVIRTUAL );
    IDENTIFIER_IS_KEYWORD( "override", OVERRIDE );
    return Token(Token::IDENTIFIER, identifierStr, line(), initial_col);
  } else if( lastChar == '"' ) {
    return getString(lastChar);
  } else {
    CHAR_IS_TOKEN(';', SEMI );
    CHAR_IS_TOKEN('.', DOT );
    CHAR_IS_TOKEN( '{', STARTBRACE );
    CHAR_IS_TOKEN( '}', ENDBRACE );
    CHAR_IS_TOKEN( '(', STARTBRACKET );
    CHAR_IS_TOKEN( ')', ENDBRACKET );
    CHAR_IS_TOKEN( ',', COMMA );
    CHAR_IS_TOKEN( '=', EQUAL );
    CHAR_IS_TOKEN_OR_TOKEN( ':', ':', UNKNOWN, COLONCOLON);
    CHAR_IS_TOKEN_OR_TOKEN( '-', '>', UNKNOWN, ARROW);
  }
  identifierStr = lastChar;
  qDebug() << "Unknown token : " << lastChar << " '" << identifierStr
           << "' at " << initial_line << "," << initial_col
           << " stream is at end? " << d->stream.atEnd()
           << " device is at end? " << d->device->atEnd()
           << " eof? " << eof()
           << " ungotten char size: " << d->ungottenChar.size();
  return Token(Token::UNKNOWN, lastChar, initial_line, initial_col);
}

QChar Lexer::getNextNonSeparatorChar()
{
  QChar lastChar;
  while(not eof() and isspace(lastChar = getNextChar()))
  { // Ignore space
  }
  if(eof())
  {
    return QChar();
  }
  return lastChar;
}

QChar Lexer::getNextChar()
{
  if(d->ungottenChar.isEmpty())
  {
    if(d->stream.atEnd())
    {
      return QChar();
    } else {
      d->stream >> d->lastChar;
      if( d->lastChar == '\n' )
      {
        ++d->line;
        ++d->followingnewline;
        d->col = 1;
      } else {
        ++d->col;
        d->followingnewline = 0;
      }
      return d->lastChar;
    }
  } else {
    return d->ungottenChar.takeLast();
  }
}

void Lexer::unget()
{
  --d->col;
  d->ungottenChar.append(d->lastChar);
  if( d->followingnewline > 0 )
  {
    --d->followingnewline;
    --d->line;
  }
}

bool Lexer::eof() const
{
  return d->ungottenChar.isEmpty() and d->stream.atEnd();
}

int Lexer::line() const
{
  return d->line;
}
int Lexer::column() const
{
  return d->col;
}

bool Lexer::ignoreComment(Token& _token, QChar _lastChar )
{
  if( _lastChar == '/' )
  {
    int initial_line = line();
    int initial_col = column();
    QChar nextChar = getNextChar();
    if( nextChar == '/' )
    { // Mono line comment
      while( not eof() and getNextChar() != '\n' )
      {
      }
      _token = nextToken();
      return true;
    } else if( nextChar == '*' )
    { // Multi line comment
      while( not eof() )
      {
        QChar nextChar = getNextChar();
        if( nextChar == '*' )
        {
          if( getNextChar() == '/' )
          {
            _token = nextToken();
            return true;
          } else {
            unget();
          }
        }
      }
      _token = Token(Token::UNFINISHED_COMMENT, initial_line, initial_col);
      return true;
    } else {
      unget();
    }
  }
  return false;
}

QString Lexer::getIdentifier(QChar lastChar)
{
  QString identifierStr;
  if( lastChar != 0) {
    identifierStr = lastChar;
  }
  while (not eof() )
  {
    lastChar = getNextChar();
    if( lastChar.isLetterOrNumber() or lastChar == '_')
    {
      identifierStr += lastChar;
    } else {
      unget();
      break;
    }
  }
  return identifierStr;
}

QString Lexer::readUntil(QChar endChar)
{
  QString str;
  while(not eof())
  {
    QChar nextChar = getNextChar();
    if(nextChar == endChar)
    {
      unget();
      return str;
    }
    str += QChar(nextChar);
  }
  return str;
}

QString Lexer::readInitialiser()
{
  int count_quote = 0;
  QChar prevChar = QChar::Null;
  
  QString str;
  while(not eof())
  {
    QChar nextChar = getNextChar();
    if(nextChar == ';' and count_quote % 2 == 0)
    {
      unget();
      return str;
    }
    else if(nextChar == '"' and prevChar != '\\')
    {
      ++count_quote;
    }
    str += nextChar;
    if(prevChar == '\\')
    {
      prevChar = QChar::Null;
    } else {
      prevChar = nextChar;
    }
  }
  return str;
}

QString Lexer::readOneArgument()
{
  QString str;
  bool in_string = false;
  bool skip = false;
  while(not eof())
  {
    QChar nextChar = getNextChar();
    if(nextChar == '\"')
    {
      in_string = !in_string;
    } else if(not in_string)
    {
      if(nextChar == '\\')
      {
        skip = true;
      } else if(skip)
      {
        skip = false;
      } else if(nextChar == ',' or nextChar == ')')
      {
        unget();
        return str;
      }
    }
    if(not skip)
    {
      str += QChar(nextChar);
    }
  }
  return str;
  
}

Token Lexer::getString(QChar lastChar)
{
  int initial_col = column();
  QChar previousChar = lastChar;
  QString identifierStr = "";
  while( not eof() )
  {
    QChar nextChar = getNextChar();
    if( nextChar == '"' and previousChar != '\\')
    {
      return Token( Token::STRING, identifierStr, line(), initial_col );
    } else {
      previousChar = nextChar;
      identifierStr += nextChar;
    }
  }
  return Token( Token::UNFINISHED_STRING, line(), initial_col);
}
