#include "Enum.h"

#include <QStringList>

#include "ClassDefinition.h"

struct Enum::Private
{
  const ClassDefinition* parent;
  QStringList values;
  bool isFlag;
};

Enum::Enum(const QString& _name, const ClassDefinition* _parent, const QStringList& _values, bool _isFlag) : Type(_name, _parent->namespaces().join("::") + "::" + _parent->name(), TType::Enum), d(new Private)
{
  d->parent = _parent;
  d->values = _values;
  d->isFlag = _isFlag;
}

Enum::~Enum()
{
  delete d;
}

const ClassDefinition * Enum::parent() const
{
  return d->parent;
}

QStringList Enum::values() const
{
  return d->values;
}

bool Enum::isFlag() const
{
  return d->isFlag;
}
