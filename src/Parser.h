/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <tuple>

#include "Token.h"
#include "ClassDefinition.h"

class Type;
class QString;

struct Error;
class Lexer;
class TypesManager;

class Parser
{
  struct ParseContext;
public:
  Parser(Lexer* _lexer, TypesManager* _manager, const QString& _filename, const QStringList& _includeDirs);
  Parser(Lexer* _lexer, ParseContext* _context, const QString& _filename, const QStringList& _includeDirs);
  ~Parser();
  std::tuple<QList<ClassDefinition*>, QStringList> parse();
  QList<Error> errors() const;
private:
  void parseTypedef(const QStringList& _namespaces);
  void parseNamespaces(const QStringList& _namespaces);
  const ClassDefinition* parseClass(const QStringList& _namespaces);
  void parseImport();
  TypesManager* manager(const QStringList& _namespaces);
private:
  const Type* parseType(const QStringList& _namespaces, const TypesManager* _currentManager, bool _accept_compound, const ClassDefinition* _currentClass);
private:
  void getNextToken();
private:
  void reportErrorOrNot(const Token& _token, const ClassDefinition::AddResult& _add_result);
  void reportError(const Token& _token, const QString& _errorMsg);
  void reportUnexpected(const Token& _token);
  void reportUnknownIndentifier(const Token& _token);
  bool isOfType(const Token& _token, Token::Type _type);
  bool isOfType(Token::Type _type);
  void reachNext(Token::Type _type);
private:
  struct Private;
  Private* const d;
};
