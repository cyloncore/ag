#include "TypeDefinition.h"

#include <QList>

struct TypeDefinition::Private
{
  QList<UnserialisationArgument> unserialisationArguments;
};

TypeDefinition::TypeDefinition(const QString& _name, const QString& _cppName, const QString& _cppArgumentName, const QString& _header) : Type(_name, _cppName, _cppArgumentName, _header, TType::TypeDef), d(new Private)
{
}

TypeDefinition::~TypeDefinition()
{
}

QList<TypeDefinition::UnserialisationArgument> TypeDefinition::unserialisationArguments() const
{
  return d->unserialisationArguments;
}


TypeDefinition::AddResult TypeDefinition::tdSuccess()
{
  return {true, QString()};
}

TypeDefinition::AddResult TypeDefinition::tdFailure(const QString& _error)
{
  return {false, _error};
}

TypeDefinition::AddResult TypeDefinition::addUnserialiseArguments(const QString& _name, const Type* _type)
{
  for(UnserialisationArgument& p : d->unserialisationArguments)
  {
    if(p.name == _name)
    {
      if(p.type == _type)
      {
        return tdSuccess();
      } else {
        return tdFailure(QString("Conflicting type for '%1'").arg(_name));
      }
    }
  }
  d->unserialisationArguments.append({_name, _type});
  return tdSuccess();
}
