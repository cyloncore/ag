#include "TestAg.h"

#include "TestAPI.h"

#include <QtTest>

void test::Test4::onFChanged()
{
  setG(f() + 1);
}

ag::Set<test::Test5*> test::Test10::f() const
{
  return ag::Set<test::Test5*>();
}

void test::Test13::initialise()
{
  setA(12);
}

void test::Test11::f(test_private::CustomTest*) {}
void test::Test11::g(const test_private::CustomTest&) {}

void test::Test17::f() {}
void test::Test18::g(int) {}

void test::Test19::initialise()
{
  setA(10);
}

TestAg::TestAg(QObject* parent) : QObject(parent)
{
}

test::Test1* TestAg::createTest1(int _offset)
{
  test::Test1* t1 = new test::Test1(this);
  fillTest1(t1, _offset);
  return t1;
}

void TestAg::fillTest1(test::Test1* _test1, int _offset)
{
  _test1->setA(_offset);
  _test1->setB(_offset + 1);
  _test1->setC(QString::number(_offset+2));
  if( (_offset + 3) % 2 == 0)
  {
    _test1->setD(test::Test1::Test::Value1);
  } else {
    _test1->setD(test::Test1::Test::Value2);
  }
}

test::Test2* TestAg::createTest2(int _offset)
{
  test::Test2* t2 = new test::Test2(this);
  fillTest2(t2, _offset);
  return t2;
}

void TestAg::fillTest2(test::Test2* _test2, int _offset)
{
  _test2->setA(_offset);
  fillTest1(_test2->b(), _offset + 1);
  QList<test::Test1*> tests1;
  for(int i = 0; i < _offset + 2; ++i)
  {
    tests1.append(createTest1(_offset + 3 + i));
  }
  _test2->setC(tests1);
}

void TestAg::testTest1()
{
  test::Test1* t1 = createTest1(0);
  QCOMPARE(t1->a(), 0);
  QCOMPARE(t1->b(), 1);
  QCOMPARE(t1->c(), "2");
  QCOMPARE(t1->d(), test::Test1::Test::Value2);
  QCOMPARE(t1->e(), test::Test1::Test::Value3);
  
  delete t1;
}

void TestAg::testTestBuilder1()
{
  test::Test1* t1 = test::Test1::create().setA(0).setB(1).setC("2").setD(test::Test1::Test::Value2);
  QCOMPARE(t1->a(), 0);
  QCOMPARE(t1->b(), 1);
  QCOMPARE(t1->c(), "2");
  QCOMPARE(t1->d(), test::Test1::Test::Value2);
  QCOMPARE(t1->e(), test::Test1::Test::Value3);
  
  delete t1;
}

void TestAg::testTest2()
{
  test::Test2* t2 = createTest2(0);
  QCOMPARE(t2->a(), 0);
  QCOMPARE(t2->b()->a(), 1);
  QCOMPARE(t2->b()->b(), 2);
  QCOMPARE(t2->b()->c(), "3");
  QCOMPARE(t2->b()->d(), test::Test1::Test::Value1);
  QCOMPARE(t2->c().size(), 2);
  QCOMPARE(t2->c()[0]->a(), 3);
  QCOMPARE(t2->c()[0]->b(), 4);
  QCOMPARE(t2->c()[0]->c(), "5");
  QCOMPARE(t2->c()[0]->d(), test::Test1::Test::Value1);
  QCOMPARE(t2->c()[1]->a(), 4);
  QCOMPARE(t2->c()[1]->b(), 5);
  QCOMPARE(t2->c()[1]->c(), "6");
  QCOMPARE(t2->c()[1]->d(), test::Test1::Test::Value2);
  
  delete t2;
}

void TestAg::testTest3()
{
  test::Test3* t3 = new test::Test3();
  t3->setA(1);
  t3->setB(2);
  t3->setC("3");
  t3->setD(test::Test1::Test::Value1);
  QCOMPARE(t3->a(), 1);
  QCOMPARE(t3->b(), 2);
  QCOMPARE(t3->c(), "3");
  QCOMPARE(t3->d(), test::Test1::Test::Value1);
  QCOMPARE(t3->e(), test::Test1::Test::Value2);
  
  delete t3;
}

void TestAg::testTestBuilder3()
{
  test::Test3* t3 = test::Test3::create().setA(0).setB(1).setC("2").setD(test::Test1::Test::Value2);
  QCOMPARE(t3->a(), 0);
  QCOMPARE(t3->b(), 1);
  QCOMPARE(t3->c(), "2");
  QCOMPARE(t3->d(), test::Test1::Test::Value2);
  QCOMPARE(t3->e(), test::Test1::Test::Value2);
  
  delete t3;
}

void TestAg::testTest4()
{
  test::Test4 t4;
  QCOMPARE(t4.f(), 4);
  QCOMPARE(t4.a(), 2);
  QCOMPARE(t4.g(), 5);
  t4.setF(3);
  QCOMPARE(t4.f(), 3);
  QCOMPARE(t4.a(), 2);
  QCOMPARE(t4.g(), 4);
}

void TestAg::testSerialisation()
{
  test::Test2* t2 = createTest2(0);
  QJsonObject obj = t2->toJson();
  QCOMPARE(obj.value("__klass__").toString(), QStringLiteral("::test::Test2"));
  test::Test2* t2_d = test::Test2::fromJson(obj);
  QVERIFY(t2_d);

  QCOMPARE(t2_d->a(), 0);
  QCOMPARE(t2_d->b()->a(), 1);
  QCOMPARE(t2_d->b()->b(), 2);
  QCOMPARE(t2_d->b()->c(), "3");
  QCOMPARE(t2_d->b()->d(), test::Test1::Test::Value1);
  QCOMPARE(t2_d->c().size(), 2);
  QCOMPARE(t2_d->c()[0]->a(), 3);
  QCOMPARE(t2_d->c()[0]->b(), 4);
  QCOMPARE(t2_d->c()[0]->c(), "5");
  QCOMPARE(t2_d->c()[0]->d(), test::Test1::Test::Value1);
  QCOMPARE(t2_d->c()[1]->a(), 4);
  QCOMPARE(t2_d->c()[1]->b(), 5);
  QCOMPARE(t2_d->c()[1]->c(), "6");
  QCOMPARE(t2_d->c()[1]->d(), test::Test1::Test::Value2);
  
  delete t2;
  delete t2_d;
}

void TestAg::testLoadable()
{
  test::Test2* t2 = createTest2(0);
  QCOMPARE(t2->filename(), QString());
  t2->setFilename("blah");
  QCOMPARE(t2->filename(), QString("blah"));
  QEXPECT_FAIL("", "Should implement some testing of loading", Continue);
  QVERIFY(t2->load());
  
  delete t2;
}

void TestAg::testSerialisationReference567()
{
  test::Test7* t7 = new test::Test7;
  t7->setFilename(":/json/test567.json");
  QString errMsg;
  QVERIFY2(t7->load(&errMsg), qPrintable(errMsg));
  QCOMPARE(t7->test5s().size(), 3);
  QCOMPARE(t7->value()->values().size(), 3);
  QCOMPARE(t7->value()->values()[0], t7->test5s().value("1"));
  QCOMPARE(t7->value()->values()[1], t7->test5s().value("4"));
  QCOMPARE(t7->value()->values()[2], t7->test5s().value("4"));
  QCOMPARE(t7->value()->values()[0]->value(), "1");
  QCOMPARE(t7->value()->values()[0]->a(), 12);
  QCOMPARE(t7->value()->values()[1]->value(), "4");
  QCOMPARE(t7->value()->values()[1]->a(), 2);
  
  delete t7;
}

void TestAg::testFlags()
{
//   test::Test1* t1 = new test::Test1;
  QCOMPARE(int(test::Test1::Flag::None), 0);
  QCOMPARE(int(test::Test1::Flag::Flag1), 1);
  QCOMPARE(int(test::Test1::Flag::Flag2), 2);
  QCOMPARE(int(test::Test1::Flag::Flag3), 4);
  QCOMPARE(int(test::Test1::Flag::All), 7);
}

void TestAg::testExternal()
{
  test::Test9 t9;
  test::Test7 t7;
  t9.setTest7(&t7);
  t9.setTest7b(&t7);
  t9.setTest7(&t7);
  t9.setTest7b(&t7);
  t9.setTest7(&t7);
  t9.setTest7b(&t7);
}

int test::Test9::answer() { return 42; }

void TestAg::testInvokable()
{
  test::Test9 t9;
  int val = 0;
  QVERIFY(QMetaObject::invokeMethod(&t9, "answer", Qt::DirectConnection, Q_RETURN_ARG(int, val)));
  QCOMPARE(val, 42);
}

void TestAg::testIniitilalise()
{
  test::Test13 t13;
  QCOMPARE(t13.a(), 12);
}

void TestAg::testVirtual()
{
  test::Test18 t18;
}

void TestAg::testClone()
{
  test::Test4 t4;
  t4.setF(10);
  t4.setB(2);
  test::Test1* t1 = &t4;
  test::Test4* t4b = dynamic_cast<test::Test4*>(t1->clone());
  QVERIFY(t4b);
  QCOMPARE(t4b->f(), 10);
  QCOMPARE(t4b->b(), 2);
  delete t4b;
}

void TestAg::testNullReference()
{
  test::Test20 t20;
  QJsonObject obj20 = t20.toJson();
  test::Test20* t20_d = test::Test20::fromJson(obj20, {});
  QCOMPARE(t20_d->t5(), nullptr);
  test::Test5 t5; t5.setValue("a");
  t20.setT5(&t5);
  obj20 = t20.toJson();
  QCOMPARE(test::Test20::fromJson(obj20, {}), nullptr);
  t20_d = test::Test20::fromJson(obj20, {&t5});
  QCOMPARE(t20_d->t5(), &t5);
}

QTEST_MAIN(TestAg)
