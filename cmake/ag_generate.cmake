function(AG_GENERATE BACKEND SOURCE DESTINATION)
  cmake_parse_arguments(ag_generate "" "" "INCLUDE_DIRS" ${ARGN})

  foreach(p ${ag_generate_INCLUDE_DIRS})
    list(APPEND EXTRA_ARGUMENTS "-I" ${p})
  endforeach()
  
  add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${DESTINATION}.cpp ${CMAKE_CURRENT_BINARY_DIR}/${DESTINATION}.h
                     DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${SOURCE} ${AG_EXECUTABLE}
                     COMMAND ${AG_EXECUTABLE} ${EXTRA_ARGUMENTS} ${BACKEND} ${CMAKE_CURRENT_SOURCE_DIR}/${SOURCE} ${CMAKE_CURRENT_BINARY_DIR}/${DESTINATION}
  )
endfunction()
