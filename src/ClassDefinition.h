/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


#include <QFlags>
#include <QList>

#include "Type.h"

class Parser;

class ClassDefinition : public Type
{
  friend class Parser;
public:
  struct Field
  {
    enum class Tag
    {
      Const = 1,
      ReadOnly = 2,
      Unserialisable = 4,
      Key = 8,
      Reference = 16,
      External = 32,
      Private = 64,
      Protected = 128
    };
    Q_DECLARE_FLAGS(Tags, Tag)
    const Type* type = nullptr;
    QString name;
    Tags tags;
    QString initialiser;
    QStringList unserialiseWith;
  };
  struct Function
  {
    enum class Tag
    {
      Constant = 1,
      Invokable = 2,
      Virtual = 4,
      PureVirtual = 8,
      Override = 16
    };
    Q_DECLARE_FLAGS(Tags, Tag)
    const Type* returnType;
    QString name;
    struct Argument
    {
      const Type* type;
      QString name;
      bool operator==(const Argument& _rhs) const { return _rhs.type == type and _rhs.name == name; }
    };
    QList<Argument> arguments;
    enum class Access { Private, Protected, Public };
    Access access;
    Tags tags;
  };
  struct UnserialisationArgument
  {
    QString name;
    const Type* type;
    bool required; ///< if true it is required to unserialise that class, otherwise, it is just available for subclasses
  };
public:
  /**
   * Forward definition
   */
  ClassDefinition(const QString& _name, const QStringList& _namespace, const TypesManager* _namespace_manager);
  ClassDefinition(const QString& _name, const QStringList& _namespace, const QStringList& _interfaces, const ClassDefinition* _parent, const TypesManager* _namespace_manager);
  ~ClassDefinition();
public:
  /**
   * @return true if it is a forward definition
   */
  bool isForwardDefinition() const;
  /**
   * Allow to swap a forward definition with the final definition
   */
  bool swap(ClassDefinition* _cd);
  const TypesManager* typesManager() const;
  const ClassDefinition* parent() const;
  /**
   * @return the type of the clone function. (this is the root of the class hierarchy).
   */
  const ClassDefinition* cloneType() const;
  QStringList namespaces() const;
  QStringList interfaces() const;
  QString name() const;
  QList<Field> fields() const;
  bool hasField(const QString& _field) const;
  bool hasKey() const;
  Field keyField() const;
  QList<Function> functions() const;
  bool hasFunction(const QString& _name) const;
  QList<const Enum*> enums() const;
  QList<const Flags*> flags() const;
  QString initialiser(const QString& _initialiser) const;
  QHash<QString, QString> initialisers() const;
  Field field(const QString& _name) const;
  QList<UnserialisationArgument> unserialisationArguments() const;
  QList<Function> pureVirtualFunctions() const;
private:
  struct AddResult {
    bool success;
    QString errorMessage;
  };
  inline static AddResult cdSuccess();
  inline static AddResult cdFailure(const QString& _error);
  AddResult addField(const Type* _type, const QString& _name, Field::Tags _tags, const QString& _initialiser, const QStringList& _unserialiseWith);
  AddResult addFunction(const Type* _returnType, const QString& _name, const QList<Function::Argument>& _arguments, Function::Access _access, Function::Tags _tags);
  AddResult addEnum(const QString& _name, const QStringList& _values, bool _is_flag);
  AddResult addFlags(const QString& _enum_name, const QString& _flags_name, const QStringList& _values);
  AddResult addInitialiser(const QString& _name, const QString& _initialiser);
  AddResult addUnserialiseArguments(const QString& _name, const Type* _type, bool _required);
private:
  struct Private;
  Private* d;
};
