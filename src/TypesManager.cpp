#include "TypesManager.h"

#include <QHash>
#include <QString>

#include "Type.h"

struct TypesManager::Private
{
  QHash<QString, const Type*> types;
  QList<const Type*> types_list;
  const TypesManager* parent;
};

#define BT(_NAME_, _CPP_NAME_) addType(new Type(_NAME_, _CPP_NAME_, _CPP_NAME_, QString(), Type::TType::BuiltIn));
#define BTT(_NAME_, _CPP_NAME_, _CPP_ARG_NAME_) addType(new Type(_NAME_, _CPP_NAME_, _CPP_ARG_NAME_, QString(), Type::TType::BuiltIn));
#define BTTH(_NAME_, _CPP_NAME_, _CPP_ARG_NAME_, _HEADER_) addType(new Type(_NAME_, _CPP_NAME_, _CPP_ARG_NAME_, _HEADER_, Type::TType::BuiltIn));

TypesManager::TypesManager(const TypesManager* _parent) : d(new Private)
{
  d->parent = _parent;
  if(d->parent == nullptr)
  {
    BT ("void", "void");
    BT ("bool", "bool");
    BT ("int", "int");
    BT ("uint8", "quint8");
    BT ("int64", "qint64");
    BT ("float", "float");
    BT ("double", "double");
    BT ("real", "qreal");
    BTT("bytes",  "QByteArray", "const QByteArray&")
    BTT("string",  "QString", "const QString&")
    BTT("variant",  "QVariant", "const QVariant&")
    BTT("datetime",  "QDateTime", "const QDateTime&")
    BTTH("point", "QPointF", "const QPointF&", "QPointF");
    BTTH("image", "QImage", "const QImage&", "QImage");
  }
}

TypesManager::~TypesManager()
{
  qDeleteAll(d->types_list);
  delete d;
}

bool TypesManager::addType(const Type* _type)
{
  if(d->types.contains(_type->name()))
  {
    return false;
  }
  d->types[_type->name()] = _type;
  d->types_list.append(_type);
  return true;
}

const Type* TypesManager::type(const QString& _name, bool _recursive) const
{
  const Type* t = d->types.value(_name, nullptr);
  return t ? t : (d->parent and _recursive) ? d->parent->type(_name, _recursive) : nullptr;
}

QList< const Type* > TypesManager::types() const
{
  return d->types_list;
}
