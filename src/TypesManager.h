#include <QList>

class QString;
class Type;

class TypesManager
{
public:
  TypesManager(const TypesManager* _parent);
  ~TypesManager();
  bool addType(const Type* _type);
  const Type* type(const QString& _name, bool _recursive) const;
  QList<const Type*> types() const;
private:
  struct Private;
  Private* const d;
};
