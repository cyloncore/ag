#include "Flags.h"

#include <QStringList>

#include "ClassDefinition.h"

struct Flags::Private
{
  const ClassDefinition* parent;
  const Enum* enum_type;
};

Flags::Flags(const QString& _flags_name, const Enum* _enum, const ClassDefinition* _parent) : Type(_flags_name, _parent->namespaces().join("::") + "::" + _parent->name(), TType::Flags),  d(new Private)
{
  d->parent = _parent;
  d->enum_type = _enum;
}

Flags::~Flags()
{
  delete d;
}

const ClassDefinition * Flags::parent() const
{
  return d->parent;
}

const Enum* Flags::enumType() const
{
  return d->enum_type;
}
